#!/usr/bin/env python

# MIT License
# Copyright (c) 2017 Massimiliano Patacchiola
#               https://mpatacchiola.github.io/blog/
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import getopt, numpy as np, os, random, sys
np.set_printoptions(precision=3)

from termcolor import colored

verbose = False

#================================================================#
# Dissecting Reinforcement Learning-Part.6 | mpatacchiola's blog #
#================================================================#

#================================================================#
def usage(x):
  print("Use:\n", x, "[-B|C -a<agent> -t<epochs> -s<episodes> -q<prints>] " \
        "[<reward-rates>]")
  print("Agents:")
  agents = os.popen("grep 'elif agent == ' %s"\
                    "|grep -v grep"\
                    "|awk '{print \" \"$4}'"%sys.argv[0]).read()
  print(agents)
  print("Default is \"Random\"")
  sys.exit()

#----------------------------------------------------------------#
def main():
  game = "multi-armed-bandit"

  # Hyper param defaults
  agent="Random"
  max_epochs = 2000 # number of trials
  max_episodes = 1000 # max length of trial
  max_quotes = 20 # total of running stats dumps desired

  # Armed-bandit defaults
  reward_rates=[0.2, 0.5, 0.8]

  # Mountain car defaults
  mass = 0.2
  friction = 0.3
  delta = 0.1

  opts, args = getopt.getopt(sys.argv[1:], "BCa:t:s:q:V?")

  for opt in opts:
    if   opt[0] == "-B": game = "multi-armed-bandit"
    elif opt[0] == "-C": game = "mountain-car"
    elif opt[0] == "-a": agent = opt[1]
    elif opt[0] == "-t": max_epochs = int(opt[1])
    elif opt[0] == "-s": max_episodes = int(opt[1])
    elif opt[0] == "-q": max_quotes = int(opt[1])
    elif opt[0] == "-V": verbose = True
    elif opt[0] == "-?": usage(sys.argv[0])
    else: sys.exit()

  if max_epochs < 1 or max_episodes < 1 or max_quotes < 0:
    print("Check your params")
    sys.exit()

  if max_quotes > max_epochs: max_quotes = max_epochs

  if len(args) > 0:
    if game == "multi-armed-bandit":
      reward_rates = list(map(float, args))
    elif game == "mountain-car":
      if len(args) > 0:
        mass = float(args[0])
        if len(args) > 1:
          friction = float(args[1])
          if len(args) > 2:
            delta = float(args[2])

  if game == "multi-armed-bandit":
    run_punter(agent, reward_rates, max_epochs, max_episodes, max_quotes)
  elif game == "mountain-car":
    run_car(agent, mass, friction, delta, max_epochs, max_episodes, max_quotes)

#================================================================#
# Multi armed bandit environment

class MultiArmedBandit:
  def __init__(self, reward_rates=[0.2, 0.5, 0.8]):
    """ Create a new bandit environment.

    @param: reward_rates: entries represent the probability of obtaining
     positive reward +1 by pulling the correspondingly numbered "arm".
     E.g., [0.2, 0.5, 0.8] defines 3 arms, the last one having probability
     0.8 of returning this reward.
    """
    self.reward_rates = reward_rates
    self.max_arms = len(reward_rates)

  def response(self, arm):
    """Pull the arm indicated.

    @param: arm an integer representing the arm to pull.
    @return: reward the reward obtained pulling that arm
    """
    assert arm >= 0 and arm < len(self.reward_rates)
    win = self.reward_rates[arm]
    lose = 1.0 - win
    return np.random.choice(2, p=[lose, win])

def softmax(x):
  '''Compute softmax values of array x.

  @param x the input array
  @return the softmax array
  '''
  return np.exp(x - np.max(x)) / np.sum(np.exp(x - np.max(x)))

def rmse(predictions, targets):
  """Return the Root Mean Square error between two arrays

  @param predictions an array of prediction values
  @param targets an array of target values
  @return the RMSE
  """
  return np.sqrt(((predictions - targets)**2).mean())

def optima(a):
  m = np.amax(a)          # maximum value in the array
  o = np.where(a == m)[0] # all indices where that maximum is attained
  return o

#================================================================#
# Agent trialling

def run_punter(agent, reward_rates, max_epochs, max_episodes, max_quotes):
  bandit = MultiArmedBandit(reward_rates=reward_rates)

  print("Running with params:", reward_rates, max_epochs, max_episodes, \
                                                     max_quotes, verbose)

  init_phase = np.log2(max_episodes)
  if init_phase < bandit.max_arms: init_phase = bandit.max_arms

  epsilon = 0.1

  epsilon_0 = 0.1
  epsilon_1 = 0.0001
  epsilons = np.linspace(epsilon_0, epsilon_1, num=max_episodes)

  temperature_0 = 0.1
  temperature_1 = 0.0001
  temperatures = np.linspace(temperature_0, temperature_1, num=max_episodes)

  reward_history = list()
  success_rates_sum = np.zeros(bandit.max_arms)

  print(agent, "agent starting...")
  for epoch in range(max_epochs):

    action_counts = np.full(bandit.max_arms, 1.0e-5)
    success_counts = np.zeros(bandit.max_arms)
    total_reward = 0

    if agent == "Bayes-Thompson": # Initial priors
      alphas = np.ones(bandit.max_arms) # 1 + success count
      betas = np.ones(bandit.max_arms)  # 1 + failure count

    for stage in range(max_episodes):
      success_rates = np.true_divide(success_counts, action_counts)

      if agent == None or len(agent) ==  0:
        pass

      elif agent == "Random":
        action = np.random.randint(low=0, high=bandit.max_arms)

      elif agent == "Softmax":
        action = np.random.choice(bandit.max_arms, p=softmax(success_rates))

      elif agent == "Greedy":
        if stage < init_phase: # Initially press every arm in turn
          action = stage%bandit.max_arms
        else: # Subsequently choose an optimal one (maximal success-rate)
          action = np.random.choice(optima(success_rates))

      elif agent == "Random-mostly-greedy":
        if stage < init_phase: # Initially press every arm in turn
          action = stage%bandit.max_arms
        else: # With small probability make a random choice
          if random.uniform(0, 1) < epsilon:
            action = np.random.randint(low=0, high=bandit.max_arms)
          else: # Else choose an optimal arm
            action = np.random.choice(optima(success_rates))

      elif agent == "Softmax-mostly-greedy":
        if stage < init_phase: # Initially press every arm in turn
          action = stage%bandit.max_arms
        else: # With small probability make an informed random choice
          if random.uniform(0, 1) < epsilon:
            action = np.random.choice(bandit.max_arms, p=softmax(success_rates))
          else: # Else choose an optimal arm
            action = np.random.choice(optima(success_rates))

      elif agent == "Random-ever-more-greedy":
        if stage < init_phase: # Initially press every arm in turn
          action = stage%bandit.max_arms
        else: # With ever decreasing probability make a random choice
          eps = epsilons[stage]
          if random.uniform(0, 1) < eps:
            action = np.random.randint(low=0, high=bandit.max_arms)
          else: # Else choose an optimal arm
            action = np.random.choice(optima(success_rates))

      elif agent == "Softmax-ever-more-greedy":
        if stage < init_phase: # Initially press every arm in turn
          action = stage%bandit.max_arms
        else: # With ever decreasing probability make an informed random choice
          eps = epsilons[stage]
          if random.uniform(0, 1) < eps:
            action = np.random.choice(bandit.max_arms, p=softmax(success_rates))
          else: # Else choose an optimal arm
            action = np.random.choice(optima(success_rates))

      elif agent == "Boltzmann":
        t = temperatures[stage]
        x = success_rates
        y = np.true_divide(x - np.max(x), t)
        z = np.exp(y) / np.sum(np.exp(y)) # Boltzmann distribution
        action = np.random.choice(bandit.max_arms, p=z)

      elif agent == "Bayes-Thompson":
        x = np.random.beta(alphas, betas) # Beta distribution
        action = np.argmax(x)

      else:
        print("No such agent:", agent)
        sys.exit()

      reward = bandit.response(action)
      assert reward == 0  or reward == 1

      action_counts[action] += 1
      success_counts[action] += reward
      total_reward += reward

      if agent == "Bayes-Thompson": # Update posteriors for subsequent priors
        alphas[action] += reward    # add success
        betas[action] += 1 - reward # add failure

    # End of epoch
    avg_reward = total_reward/max_episodes
    reward_history.append(avg_reward)
    success_rates_sum += success_rates

    if max_quotes > 0 and epoch % (max_epochs//max_quotes) == 0:
      print("Epoch", epoch, "| Avg reward", avg_reward)
      print("Success rates", success_rates)
      print("RMSE to prior", "%.3f"%rmse(success_rates, reward_rates))

  # End of time
  avg_success_rates = success_rates_sum/max_epochs
  mean_rewards = np.mean(reward_history)
  std_rewards = np.std(reward_history)
  print(" Mean avg rewards", colored("%.3f"%mean_rewards, "green"), "<", agent)
  print(" Std  avg rewards", "%.3f"%std_rewards)
  print("Avg success rates", avg_success_rates)
  print("    RMSE to prior", "%.3f"%rmse(avg_success_rates, reward_rates))

#================================================================#
# Mountain car environment

class MountainCar:
  Left = -1; Drift = 0; Right = 1
  moves = [ Left, Drift, Right ]
  num_moves = len(moves)
  x_min = -1.2; x_init = -0.5; v_init =  0.0; x_max = 0.5

  def __init__(self, mass=0.2, friction=0.3, delta=0.1):
    """ Create a new mountain car object.

    @param mass: the mass of the car
    @param friction: the friction in Newtons
    @param delta: the time step in seconds
    """
    self.gravity = 9.8
    self.friction = friction
    self.delta = delta  # time-delta, in seconds
    self.mass = mass
    self.x_cur = self.x_init
    self.v_cur = self.v_init
    self.history = []

  def reset(self, exploring_starts=True, init_pos=-0.5, init_vel=0.0):
    """ Resets the car to an initial position [-1.2, 0.5]

    @param exploring_starts: if True a random position is taken
    @param init_pos: the initial position of the car (requires exploring_starts=False)
    @return: return the initial position and velocity of the car
    """
    if exploring_starts: init_pos = np.random.uniform(-1.2,0.5)
    if   init_pos < self.x_min: init_pos = self.x_min
    elif init_pos > self.x_max: init_pos = self.x_max
    self.x_cur = init_pos
    self.v_cur = init_vel
    self.history = [init_pos]
    return [self.x_cur, self.v_cur]

  def step(self, move):
    """Perform one step in the environment following the action.

    @param action: an integer representing one of three moves,
     -1 = left, 0 = nowhere, 1 = right
    @return: (x_new, v_new), reward, done
     where reward is negative and done False until the goal is reached
    """
    assert move in self.moves
    pull = self.gravity * self.mass * np.cos(3*self.x_cur)
    drag = self.friction * self.v_cur
    v_new = self.v_cur + (move - pull -drag) * self.delta
    x_new = self.x_cur + (v_new * self.delta)
    # Check boundary condition (car outside frame)
    if x_new < self.x_min:
      x_new = self.x_min
      v_new = self.v_init
    # Adopt new position and velocity
    self.x_cur = x_new
    self.v_cur = v_new
    self.history.append(x_new)
    # Issue reward and declare end when the car reaches the goal
    reward = -0.01
    done = False
    if x_new >= 0.5:
      reward = 1.0
      done = True
    return [x_new, v_new], reward, done

#================================================================#
# Pygame setup

import pygame, time

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
BLUE = (0, 0, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)

#================================================================#
# Agent trialling

def run_car(agent, mass, friction, delta, max_epochs, max_episodes, max_quotes):
  car = MountainCar(mass, friction, delta)
  pygame.init()
  font = pygame.font.SysFont("Calibri", 16, True, False)
  screen = pygame.display.set_mode((400, 400))
  pygame.display.set_caption("Mountain Car (" + agent + ")")
  clock = pygame.time.Clock()

  print(agent, "agent starting")
  for epoch in range(max_epochs):

    total_reward = 0
    x_new, v_new = car.reset()
    x_cur = v_cur = cur_move = None

    print("Starting at %.3f"%x_new, "with speed %.3f"%v_new)
    for stage in range(max_episodes):

      for event in pygame.event.get():
        if event.type == pygame.QUIT:
          done = True
        elif event.type == pygame.KEYDOWN:
          if event.key == pygame.K_q:
            done = True

      screen.fill(BLACK)

      y_new = np.sin(3 * x_new)
      xs_new = int(200 + 50 * x_new) # screen coords of x_new
      ys_new = int(200 - 50 * y_new) # ... and y_new
      pygame.draw.circle(screen,RED,[xs_new,ys_new],3,3)

      if x_cur is not None:
        y_cur = np.sin(3 * x_cur)
        xs_cur = int(200 + 50 * x_cur)
        ys_cur = int(200 - 50 * y_cur)
        pygame.draw.circle(screen,BLUE,[xs_cur,ys_cur],3,3)
        pygame.draw.line(screen, WHITE, [xs_new,ys_new], [xs_cur,ys_cur], 2)

      data = "( " + str("%.3f"%x_new) + " , " + str("%.3f"%y_new) + " )"
      if   cur_move == car.Left:  data += " <<< "
      elif cur_move == car.Drift: data += " ooo "
      elif cur_move == car.Right: data += " >>> "
      text = font.render(data, True, WHITE)
      screen.blit(text, [0,0])
      pygame.display.flip()
      clock.tick(5)

      if agent == None or len(agent) ==  0:
        pass

      elif agent == "Random":
        new_move = random.randint(car.Left,1)

      elif agent == "Opportunist":
        new_move = car.Right
        if cur_move is None:
          if random.randint(0,1) == 0: new_move = car.Left
        else:
          new_move = cur_move
          if (x_new < x_cur and cur_move == car.Right) \
          or (x_new > x_cur and cur_move == car.Left): new_move = -cur_move

      elif agent[:8] == "Swinger1":
        new_move = 0
        if x_cur is not None:
          if x_new < x_cur:
            if (agent[-5:] == "-busy" and y_new > y_cur) \
            or (agent[-5:] == "-lazy" and y_new < y_cur): new_move = car.Left
          elif x_new > x_cur:
            if (agent[-5:] == "-busy" and y_new > y_cur) \
            or (agent[-5:] == "-lazy" and y_new < y_cur): new_move = car.Right
          elif x_new == x_cur:
            if cur_move is None: new_move = car.Right
            else:                new_move = - cur_move

      elif agent[:8] == "Swinger2":
        new_move = 0
        if x_cur is not None:
          if (agent[-5:] == "-busy" and y_new > y_cur) \
          or (agent[-5:] == "-lazy" and y_new < y_cur):
            if   x_new < x_cur: new_move = car.Left
            elif x_new > x_cur: new_move = car.Right
          if new_move == 0 and x_new == x_cur:
            if cur_move is None: new_move = car.Right
            else:                new_move = - cur_move

      else:
        print("No such agent:", agent)
        sys.exit()

      x_cur = x_new
      v_cur = v_new
      y_cur = y_new
      cur_move = new_move
      result, reward, done = car.step(cur_move)
      x_new, v_new = result
      total_reward += reward
      if done: break

    print("Finished after stage", stage)
    print("Total reward: %.3f"%total_reward)
    time.sleep(1)

  pygame.quit()

#================================================================#
if __name__ == "__main__":
  main()

# vim:set ts=2 sw=2 expandtab indentexpr=
