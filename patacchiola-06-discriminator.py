#!/usr/bin/env python

# MIT License
# Copyright (c) 2017 Massimiliano Patacchiola
#               https://mpatacchiola.github.io/blog/
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

#================================================================#
import getopt, numpy as np, sys
np.set_printoptions(precision=3)

verbose = False

#----------------------------------------------------------------#
# Dissecting Reinforcement Learning-Part.7 | mpatacchiola's blog #
#----------------------------------------------------------------#

#----------------------------------------------------------------#
# Tuning params
gamma = 0.999 # future rewards discount; rewards further ahead are less relevant
max_epochs = 100000 # max number of trials
max_episodes = 100 # max length of trial
max_quotes = 100 # total number of intermediate stats dumps desired

#================================================================#

def main():
  global verbose, gamma, max_epochs, max_episodes, max_quotes
  X = Y = 5;
  bias = None # defaults differently for linear vs quadratic approx
  num_ins = 2; num_hidds = 4; num_outs = 1; activation = "sigmoid"
  inputs = target = None; max_train_steps = 1 # only for self-tests

  opts, args = getopt.getopt(sys.argv[1:], "G:I:O:S:a:n:b:i:I:o:O:h:t:s:q:V", [
      "activation="
    ]
  )
  for opt in opts:
    if   opt[0] == "-a": algo = int(opt[1])
    elif opt[0] == "-n": X, Y = map(int, opt[1].split("x"))
    elif opt[0] == "-b": bias = float(opt[1])
    elif opt[0] == "-i": num_ins = int(opt[1])
    elif opt[0] == "-o": num_outs = int(opt[1])
    elif opt[0] == "-h": num_hidds = int(opt[1])
    elif opt[0] == "-t": max_epochs = int(opt[1])
    elif opt[0] == "-s": max_episodes = int(opt[1])
    elif opt[0] == "-q": max_quotes = int(opt[1])
    elif opt[0] == "-V": verbose = True
    elif opt[0] == "--activation": activation = opt[1]
    elif opt[0] == "-G": gamma = float(opt[1])
    elif opt[0] == "-I": inputs = list(map(float,opt[1].split(",")))
    elif opt[0] == "-O": target = list(map(float,opt[1].split(",")))
    elif opt[0] == "-S": max_train_steps = int(opt[1])

  if max_quotes > max_epochs: max_quotes = max_epochs
  assert target is None or (min(target) >= -1.0 and 1.0 >= max(target))

  if   algo == 0: algo_TD0_tab(X,Y)
  elif algo == 1: algo_TD0_lin(X,Y,bias)
  elif algo == 2: algo_TD0_quad(X,Y,bias)
  elif algo == 3: algo_MLP(X,Y,num_ins,num_hidds,num_outs,activation)
  elif algo == 9: test_MLP(inputs,num_hidds,target,max_train_steps,activation)

#================================================================#
# Environment: Four "boolean worlds", of dimension X x Y
WORLDS = [ "AND", "NAND", "OR", "XOR" ]

# State and reward matrices for the four boolean worlds
# NB: Matrix[row=x,col=y] encodes data for coordinate (x,y)

def boolean_states_and_rewards(X, Y):
  States = np.zeros((X,Y))
  States[0,0] = States[0,Y-1] = States[X-1,0] = States[X-1,Y-1] = 1

  # Reward matrices
  RW = []
  for i in range(len(WORLDS)): RW.append(np.zeros((X,Y)))
  #                           REWARDS                          in the
  #     0/AND         1/NAND          2/OR          3/XOR      WORLD
  # +-----------+  +-----------+  +-----------+  +-----------+
  # |-1   0s  +1|  |+1   0s  -1|  |+1   0s  +1|  |+1   0s  -1|
  # | 0s  0s  0s|  | 0s  0s  0s|  | 0s  0s  0s|  | 0s  0s  0s|
  # |-1   0s  -1|  |+1   0s  +1|  |-1   0s  +1|  |-1   0s  +1|
  # +-----------+  +-----------+  +-----------+  +-----------+
  RW[0][0,0] = -1; RW[0][0,Y-1] = -1; RW[0][X-1,0] = -1; RW[0][X-1,Y-1] =  1
  RW[1][0,0] =  1; RW[1][0,Y-1] =  1; RW[1][X-1,0] =  1; RW[1][X-1,Y-1] = -1
  RW[2][0,0] = -1; RW[2][0,Y-1] =  1; RW[2][X-1,0] =  1; RW[2][X-1,Y-1] =  1
  RW[3][0,0] = -1; RW[3][0,Y-1] =  1; RW[3][X-1,0] =  1; RW[3][X-1,Y-1] = -1

  return States, RW

#----------------------------------------------------------------#
# Print data held for (x,y) at euclidean coordinate (x,y) on a screen patch
# rather than on row x, column y (i.e., un-rotate it)
def print_xy(fmt, M):
  #print(np.flipud(M))
  X, Y = M.shape
  for y in reversed(range(Y)):
    for x in range (X):
      print(fmt%M[x,y], end="")
    print()

#----------------------------------------------------------------#
# GridWorld.pos encodes data for points at coordinates (x,y)

class GridWorld:
  # Four actions, too
  UP = 0; LEFT = 1; DOWN = 2; RIGHT = 3
  actions = [UP, LEFT, DOWN, RIGHT]

  # Model uncertainty of compliance ("Transition-model")
  # T[row=i,col=j] = Probability(action j results | action i was mandated)
  Transitions = np.array([[0.8, 0.1, 0.0, 0.1],
                          [0.1, 0.8, 0.1, 0.0],
                          [0.0, 0.1, 0.8, 0.1],
                          [0.1, 0.0, 0.1, 0.8]])

  def __init__(self, X, Y):
    self.nA = len(self.actions)
    self.X = X
    self.Y = Y
    self.pos = [np.random.randint(X), np.random.randint(Y)]
    self.Rewards = [None] * len(WORLDS)
    self.States, self.Rewards = boolean_states_and_rewards(X,Y)

  def render(self):
    graph = ""
    for y in reversed(range(self.Y)):
      for x in range(self.X):
        if   self.pos == [x,y]:      graph += u" \u25CB " # u" \u25CC "
        elif self.States[x,y] ==  0: graph += ' - '
        elif self.States[x,y] == -1: graph += ' # '
        elif self.States[x,y] == +1: graph += ' * '
      graph += '\n'
    print(graph)

  def reset(self, exploring_starts=False, x=None, y=None):
    if exploring_starts:
      while True:
        x = np.random.randint(0, self.X)
        y = np.random.randint(0, self.Y)
        if self.States[x,y] == 0: break
      self.pos = [x,y]
    elif x is None or y is None:
      #self.pos = [self.X//2, self.Y//2] # away from the corners
      self.pos = [np.random.randint(self.X), np.random.randint(self.Y)]
    else:
      self.pos = [x, y]
    return self.pos

  def step(self, action):
    assert action in self.actions
    # "action = Transition-model(action)"
    action = np.random.choice(4, 1, p=self.Transitions[int(action),:])
    # Get new position, rewards and "done?" from current position and action
    done = False
    rewards = [0] *len(WORLDS) # one per world
    x, y = self.pos
    u = v = -1
    if   action == self.UP   : u, v = [x,   y+1]
    elif action == self.RIGHT: u, v = [x+1, y  ]
    elif action == self.DOWN : u, v = [x,   y-1]
    elif action == self.LEFT : u, v = [x-1, y  ]
    # Validate new position
    if u >= 0 and u < self.X and v >= 0 and v < self.Y \
                             and self.States[u,v] != -1:
      self.pos = [u, v]
      rewards = [self.Rewards[i][u,v] for i in range(len(WORLDS))]
      done = bool(self.States[u,v]) # state is terminal
    return self.pos, rewards, done

#================================================================#
# Algorithms:

# TD(0) tabular algorithm for utility discrimination in boolean worlds

#----------------------------------------------------------------#
# Utilities update
def upd_util_TD0_tab(util, cur_obs, new_obs, alpha, gamma, reward, done):
  x, y = cur_obs; Q_cur = util[x,y]
  if done:
    util[x,y] += alpha * (reward - Q_cur)
  else:
    u, v = new_obs; Q_new = util[u,v]
    util[x,y] += alpha * (reward - Q_cur + gamma * Q_new)
  return util

#----------------------------------------------------------------#
# NB: World scenarios are run in parallel
# This is possible since actions are not influenced by outcomes

def algo_TD0_tab(X,Y):
  global gamma, max_epochs, max_episodes, max_quotes
  alpha = 0.1 # fixed learning rate (speed of adopting changes)
  env = GridWorld(X,Y)
  utils = [None, None, None, None]
  for i in range(len(WORLDS)): # AND, NAND, OR, XOR
    utils[i] = np.zeros((X,Y)) # utilities
  for epoch in range(max_epochs):
    new_obs = env.reset(exploring_starts=False)
    for _ in range(max_episodes):
      cur_obs = new_obs
      action = np.random.randint(0,env.nA) # always random
      new_obs, rewards, done = env.step(action)
      for i in range(len(WORLDS)):
        utils[i] = upd_util_TD0_tab(utils[i], cur_obs, new_obs, \
                                    alpha, gamma, rewards[i], done)
      if done: break

    if epoch+1 == max_epochs or (epoch+1) % (max_epochs//max_quotes) == 0:
      print("Utilities after epoch", epoch+1)
      for i in range(4):
        print("%5s"%WORLDS[i])
        print_xy("%+.3f ", utils[i])

#================================================================#
# TD(0) linear algorithm for utility discrimination in boolean worlds

#----------------------------------------------------------------#
# Update weight vector from current and new states
def upd_weights_TD0(w, cur_s, new_s, alpha, gamma, reward, done):
  if done:
    w += alpha * cur_s * (reward - np.dot(cur_s,w))
  else:
    w += alpha * cur_s * (reward - np.dot(cur_s,w) + gamma*np.dot(new_s,w))
  return w

#----------------------------------------------------------------#
# Generate utilities from weights and current state (linear mode)
def gen_util_TD0_lin(w, X, Y):
  util = np.zeros((X, Y))
  for x in range(X):
    for y in range(Y):
      s = np.ones(w.shape[0])
      s[0], s[1] = x, y
      util[x,y] = np.dot(s,w)
  print_xy("%+.3f ", util)

#----------------------------------------------------------------#

# NB: World scenarios are run in parallel
def algo_TD0_lin(X,Y,bias=0):
  global gamma, max_epochs, max_episodes, max_quotes
  alpha0 = 0.001 # initial learning rate
  alpha1 = 0.00000001 # final
  alphav = np.linspace(alpha0, alpha1, max_epochs) # learning rate (d)escalator
  if bias is None: bias = 0.0

  wghts = [None] * len(WORLDS)
  mse_hist = [None] * len(WORLDS)

  for i in range(len(WORLDS)): # AND, NAND, OR, XOR
    wghts[i] = np.random.uniform(-1, 1, 3) # the 3rd gets ignored if bias is 0
    mse_hist[i] = [] # track history for plots

  env = GridWorld(X,Y)

  total_steps = 0
  for epoch in range(max_epochs):
    alpha = alphav[epoch]
    new_obs = env.reset(exploring_starts=True)
    # A state is represented by three features: x, y, bias (two, if bias == 0)
    new_s = np.array(new_obs + [bias])
    mse = [None] * len(WORLDS)
    for i in range(len(WORLDS)): mse[i] = 0.0
    for stage in range(max_episodes):
      cur_obs = new_obs
      cur_s = new_s
      action = np.random.randint(0,env.nA)
      new_obs, rewards, done = env.step(action)
      new_s = np.array(new_obs + [bias])
      for i in range(len(WORLDS)):
        # Update weights and estimate MSE for subsequent plots
        wghts[i] = upd_weights_TD0(wghts[i], cur_s, new_s, \
                                   alpha, gamma, rewards[i], done)
        mse[i] += (np.dot(new_s,wghts[i]) - np.dot(cur_s,wghts[i]))**2
      total_steps += 1
      if done: break

    for i in range(len(WORLDS)):
      mse[i] /= stage + 0.000000001
      if stage > 0: mse_hist[i].append(mse[i])

    if epoch+1 == max_epochs or (epoch+1) % (max_epochs//max_quotes) == 0:
      print("Epoch", epoch+1, "total steps", total_steps, "alpha %.3f"%alpha)
      for i in range(len(WORLDS)):
        print("%5s"%WORLDS[i], "world: MSE %.3f"%mse[i], "weights", wghts[i])
        gen_util_TD0_lin(wghts[i], X, Y)

  print("Generating plots...")
  plot_utility(wghts, bias, None, X, Y, \
               filename="/tmp/boolean-planes.png")

  for i in range(len(WORLDS)):
    plot_errors(mse_hist[i], filepath="/tmp/mse-linear-"+WORLDS[i]+".png",
                x_label="iterations", y_label="MSE",
                x_range=(0, len(mse_hist[i])), y_range=(0,1),
                color="-r", kernel_size=50, alpha=0.4, grid=True)

#================================================================#
# TD(0) quadratic algorithm for utility discrimination in boolean worlds

#----------------------------------------------------------------#
# Generate utilities from weights and current state (quadratic mode)
def gen_util_TD0_quad(w, X, Y, bias):
  util = np.zeros((X, Y))
  for x in range(X):
    for y in range(Y):
      s = np.ones(w.shape[0])
      s[0] = x
      s[1] = y
      s[2] = x * y
      s[3] = bias
      util[x,y] = np.dot(s,w)
  print_xy("%+.3f ", util)

#----------------------------------------------------------------#
def algo_TD0_quad(X,Y,bias=1.0):
  global gamma, max_epochs, max_episodes, max_quotes
  alpha0 = 0.001
  alpha1 = 0.0000001
  alphav = np.linspace(alpha0, alpha1, max_epochs)
  if bias is None: bias = 1.0

  env = GridWorld(X,Y)
  # Weights are randomly initialized in range -1.0 .. +1.0
  wghts = [None] * len(WORLDS)
  mse_hist = [None] * len(WORLDS)

  for i in range(len(WORLDS)):
    wghts[i] = np.random.uniform(-1.0, 1.0, 4)
    mse_hist[i] = []

  total_steps = 0
  for epoch in range(max_epochs):
    alpha = alphav[epoch]
    new_obs = env.reset(exploring_starts=True)
    x, y = new_obs
    # A state is represented by four features: x, y, x*y, bias
    new_s = np.array([x, y, x*y, bias])
    mse = [None] * len(WORLDS)
    for i in range(len(WORLDS)): mse[i] = 0.0
    for stage in range(max_episodes):
      cur_obs = new_obs
      cur_s = new_s
      action = np.random.randint(0, env.nA)
      new_obs, rewards, done = env.step(action)
      x, y = new_obs
      new_s = np.array([x, y, x*y, bias])
      for i in range(4):
        wghts[i] = upd_weights_TD0(wghts[i], cur_s, new_s, \
                                   alpha, gamma, rewards[i], done)
        mse[i] += (np.dot(new_s,wghts[i]) - np.dot(cur_s,wghts[i]))**2
      total_steps += 1
      if done: break

    for i in range(len(WORLDS)):
      mse[i] /= stage + 0.000000001
      if stage > 0: mse_hist[i].append(mse[i])

    if epoch+1 == max_epochs or (epoch+1) % (max_epochs//max_quotes) == 0:
      print("Epoch", epoch+1, "total steps", total_steps, "alpha %.3f"%alpha)
      for i in range(len(WORLDS)):
        print("%5s"%WORLDS[i], "world: MSE %.3f"%mse[i], "weights", wghts[i])
        gen_util_TD0_quad(wghts[i], X, Y, bias)

  print("Generating plots...")
  plot_utility(wghts, bias, None, X, Y, \
               filename="/tmp/boolean-parabolas.png")

  for i in range(len(WORLDS)):
    plot_errors(mse_hist[i], filepath="/tmp/mse-quadratic-"+WORLDS[i]+".png",
                x_label="iterations", y_label="MSE",
                x_range=(0, len(mse_hist[i])), y_range=(0,1),
                color="-r", kernel_size=50, alpha=0.4, grid=True)

#================================================================#

#----------------------------------------------------------------#
# Dissecting Reinforcement Learning-Part.8 | mpatacchiola's blog #
#----------------------------------------------------------------#

# Discriminating boolean states with multi-layered perceptrons

class MLP():

  # Available activations
  def linear(self, z):  return np.minimum(1.0, np.maximum(z, -1.0))
  def sigmoid(self, z): return 2.0 / (1.0 + np.exp(-z)) - 1.0
  def tanh(self, z):    return np.tanh(z)

  # Their derivatives
  def linear_grad(self, z):  return 1.0
  def sigmoid_grad(self, z): return 0.5 - 0.5*np.square(np.tanh(0.5*z)) # sech^2(z/2)/2
  def tanh_grad(self, z):    return 1.0 - np.square(np.tanh(z)) # sech^2(z)

  # Fix choices at the outset
  ACTV_refs = ["linear",    "sigmoid",    "tanh"]
  ACTV_funcs = [linear,      sigmoid,      tanh]
  ACTV_grads = [linear_grad, sigmoid_grad, tanh_grad]

  tolerance = 0.001

  def __init__(self, num_ins, num_hidds, num_outs, activation="sigmoid"):
    '''Init an MLP object

    Defines the weight matrices associated with the MLP.
    @param num_ins: total number of inputs
    @param num_hidds: total hidden units
    @param num_outs: total number of outputs
    @param activation: the activation function [sigmoid, tanh]
    '''
    assert activation in self.ACTV_refs
    self.ACTV_id = self.ACTV_refs.index(activation)
    self.ACTV_func = self.ACTV_funcs[self.ACTV_id]
    self.ACTV_grad = self.ACTV_grads[self.ACTV_id]
    self.num_ins = num_ins
    self.W1 = np.random.normal(0.0, 0.1, (num_ins+1, num_hidds))
    self.W2 = np.random.normal(0.0, 0.1, (num_hidds+1, num_outs))
    self.num_outs = num_outs

  def forward(self, x):
    '''Forward pass in the neural network via input propagation applying
    weights

    @param x: the input vector (must have shape==num_ins)
    @return: y the output of the network
    '''
    assert x.shape[0] == self.num_ins
    self.x = np.hstack([x, np.array([1.0])]) # add bias unit
    self.z1 = np.dot(self.x, self.W1)
    self.h = self.ACTV_func(self, self.z1)
    self.h = np.hstack([self.h, np.array([1.0])]) # add bias unit
    self.z2 = np.dot(self.h, self.W2) # shape [num_outs]
    self.y = self.ACTV_func(self, self.z2)
    if verbose:
      print("x ")
      print(x)
      print("W1")
      print(self.W1)
      print("z1")
      print(self.z1)
      print(" h")
      print(self.h)
      print("W2")
      print(self.W2)
      print("z2")
      print(self.z2)
      print(" y")
      print(self.y)
      input()
    return self.y

  def backward(self, y, target):
    '''Backward pass in the network via gradient (punish weights in accordance
    with their contribution to the error)

    @param y: the output of the network
    @param target: the value of taget vector (of same shape as output)
    @return: the gradient vectors for the two weight matrices
    '''
    assert y.shape == target.shape
    # Collect required derivatives
    # E = error = (y - target)^2/2, so dE/dy = y - target
    dE_dy = y - target # shape = [num_outs]
    # y = f(z2), so dy/dz2 = df/dz2 where f = activation
    dy_dz2 = self.ACTV_grad(self, self.z2) # shape = [num_outs]
    # z2_k = sum_j h_jW2_{jk}, so dz2_k/dW2_{jk} = h_j
    dz2_dW2 = self.h # shape = [num_hidds + 1].T
    # z2_k = sum_j h_jW2_{jk}, so dz2_k/dh_j = W2_{jk}
    dz2_dh = self.W2 # shape = [num_outs] * [num_hidds + 1]
    # h = f(z1), so dh/dz1 = df/dz1
    dh_dz1 = self.ACTV_grad(self, self.z1) # shape = [num_hidds].T
    # z1_k = sum_j x_jW1_{jk}, so dz1_k/dW1_{jk} = x_k
    dz1_dW1 = self.x # shape = [num_ins + 1]

    # Chain rule for descent on W2
    # dE_dW2 = dE_dy * dy_dz2 * dz2_dW2
    # shape = [num_outs] * [num_hidds]
    dE_dW2 = np.dot(np.expand_dims(dE_dy * dy_dz2, axis=1),
                    np.expand_dims(dz2_dW2, axis=0)).T

    # Chain rule for descent on W1
    # dE_dW1 = dE_dy * dy_dz2 * dz2_dh * dh_dz1 * dz1_dW1
    dE_dz2 = dE_dy * dy_dz2 # shape = [num_outs]
    dE_dh = np.dot(dE_dz2, dz2_dh.T) # shape = [num_hidds + 1]
    dE_dh = dE_dh[0:-1] # remove bias: [num_hidds]
    # shape = [num_hidds] * [num_ins+1]
    dE_dW1 = np.dot(np.expand_dims(dE_dh *dh_dz1,axis=1),
                    np.expand_dims(dz1_dW1,axis=0)).T
    if verbose:
      print("y")
      print(self.y)
      print("target")
      print(target)
      print("dE_dW1")
      print(dE_dW1)
      print("dE_dW2")
      print(dE_dW2)
      input()
    return dE_dW1, dE_dW2

  def train(self, x, target, alpha=0.1, max_steps=100):
    '''Train the network and update its weights

    @param x: the input vector
    @param target: the target vector
    @param alpha: the learning rate (default 0.1)
    @return: the error (MSE)
    '''
    for _ in range(max_steps):
      y = self.forward(x)
      dE_dW1, dE_dW2 = self.backward(y, target)
      # update the weights
      self.W2 = self.W2 - (alpha * dE_dW2)
      self.W1 = self.W1 - (alpha * dE_dW1)
      # estimate the error
      error = 0.5 * (target - y)**2
      if verbose:
        print("error")
        print(error)
        input()
      if (error < self.tolerance).all(): break;
    return error

#----------------------------------------------------------------#

def upd_weights_MLP(mlp, cur_obs, new_obs, alpha, gamma, reward, done):
  '''Adjust the network to "predict" the utility of the new state from the
  current state

  @param cur_obs features observed before the action
  @param new_obs features observed after the action
  @param reward the reward obtained after the action
  @param alpha the learning rate
  @param gamma the future discount factor
  @param done boolean True if the state is terminal
  @return the udated weights vector and errors
  '''
  x_cur = np.array(cur_obs, dtype=np.float32)
  x_new = np.array(cur_obs, dtype=np.float32)
  if done:
    target = np.array([reward], dtype=np.float32)
  else:
    target = np.array((reward + (gamma * mlp.forward(x_new))), dtype=np.float32)
  error = mlp.train(x_cur, target, alpha)
  return mlp, error

#----------------------------------------------------------------#
def gen_util_MLP(mlp, X, Y):
  '''Print the utility matrix of a discrete coordinate state space

  @param mlp an MLP object having single output
  @param X total number of rows
  @param Y total number of columns
  '''
  util = np.zeros((X, Y))
  for x in range(X):
    for y in range(Y):
      z = np.array([x,y], dtype=np.float32)
      util[x,y] = mlp.forward(z)
  print_xy("%+.3f ", util)

#----------------------------------------------------------------#
def algo_MLP(X, Y, num_ins, num_hidds, num_outs, activation):
  assert num_ins == 2 and num_outs == 1 # TBD; meanwhile more hiddens may help

  global gamma, max_epochs, max_episodes, max_quotes
  alpha = 0.1 # fixed learning rate (speed of adopting changes)

  env = GridWorld(X,Y)

  mlps = [None] * len(WORLDS)
  errors = [None] * len(WORLDS)
  for i in range(len(WORLDS)):
    mlps[i] = MLP(num_ins=num_ins, num_hidds=num_hidds, num_outs=num_outs, \
                  activation=activation)

  total_steps = 0
  for epoch in range(max_epochs):
    new_obs = env.reset(exploring_starts=True)
    for stage in range(max_episodes):
      cur_obs = new_obs
      action = np.random.randint(0,4)
      new_obs, rewards, done = env.step(action)
      for i in range(len(WORLDS)):
        mlps[i], errors[i] = upd_weights_MLP(mlps[i], cur_obs, new_obs,
                                             alpha, gamma, rewards[i], done)
      total_steps += 1
      if done: break

    if epoch+1 == max_epochs or (epoch+1) % (max_epochs//max_quotes) == 0:
      print("Epoch", epoch+1, "total steps", total_steps)
      for i in range(len(WORLDS)):
        print("%5s"%WORLDS[i], "error %.3f"%errors[i])
        gen_util_MLP(mlps[i], X, Y)

  print("Generating plots...")
  plot_utility(None, None, mlps, X, Y, \
               filename="/tmp/boolean-surfaces.png")

#----------------------------------------------------------------#
def test_MLP(inputs, num_hidds, target, max_train_steps, activation):
  if inputs is None: inputs = map(float, input("Inputs: "))
  if target is None: target = map(float, input("Targets: "))
  x = np.array(inputs)
  target = np.array(target)
  num_ins = len(x)
  num_outs = len(target)
  mlp = MLP(num_ins, num_hidds, num_outs, activation)
  i = 0
  while True:
    y = mlp.forward(x)
    print("Step", i)
    print("x     ", x)
    print("target", target)
    print("y     ", y)
    dE_dW1, dE_dW2 = mlp.backward(y, target)
    print("dE_dW1")
    print(dE_dW1)
    print("dE_dW2")
    print(dE_dW2)
    print("W1")
    print(mlp.W1)
    print("W2")
    print(mlp.W2)
    error = mlp.train(x, target, alpha=0.1, max_steps=max_train_steps)
    print("error", error, "tolerance", mlp.tolerance)
    if (error < mlp.tolerance).all():
      print("Converged")
      print("x     ", x)
      print("target", target)
      print("y     ", mlp.y)
      break;
    i += max_train_steps
    if "q" == input(): break

#================================================================#
# Plots (verbose bits, relegated to the bottom)

import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import cm
from matplotlib.patches import Rectangle

import mpl_toolkits.mplot3d.art3d as art3d
from mpl_toolkits.mplot3d import Axes3D

# Represent utility as a surface over the boolean world

def plot_utility(wghts, bias, mlps, X, Y, filename="/tmp/plot_utility.png"):

  # Specify either "weights" or "MLPs"
  assert wghts is not None or mlps is not None
  assert (wghts is None and bias is None) or mlps is None
  if wghts is not None and bias is None: bias = 0.0 # default unspecified bias

  # Main figure property
  fig, ax = plt.subplots(nrows=1, ncols=len(WORLDS), \
    subplot_kw={'projection': '3d', 'autoscale_on':False}) # , 'aspect':'equal'})
  colours = [] #   0,0     0,1    1,0      1,1      # j,k
  colours.append(["red"  ,"red",    "red","green"]) # AND
  colours.append(["green","green","green",  "red"]) # NAND
  colours.append(["red"  ,"green","green","green"]) # OR
  colours.append(["red"  ,"green","green",  "red"]) # XOR
  azim = [115, 65, 45, 25]

  for i in range(len(WORLDS)):
    # Subplot for WORLD i
    ax[i].clear()
    # Draw base rectangles
    for j in range(2):
      for k in range(2):
        color = colours[i][2*j+k]
        x, y = (X-1)*j, (Y-1)*k
        p = Rectangle((x,y), 1, 1, color=color, alpha=0.5)
        ax[i].add_patch(p)
        art3d.pathpatch_2d_to_3d(p, z=-1.0, zdir="z")
    # Set the grid
    ax[i].set_xticks(np.arange(0, X+1, 1))
    ax[i].set_yticks(np.arange(0, X+1, 1))
    ax[i].set_xticklabels('', fontsize=10)
    ax[i].set_yticklabels('', fontsize=10)
    ax[i].set_zlim(-1.0,1.0)
    ax[i].set_zticklabels(['-1.0','','0','','1.0'], fontsize=10)
    ax[i].view_init(elev=30, azim=-azim[i])

    # Plot surface point (x, y, z = utility(x,y))
   #x_vec = np.arange(0.5, X-0.5, 0.5) # coarser
   #y_vec = np.arange(0.5, X-0.5, 0.5)
    x_vec = np.arange(0.0, X-1.0, 0.01) # finer
    y_vec = np.arange(0.0, Y-1.0, 0.01)
    x, y = np.meshgrid(x_vec, y_vec)

    # Get the z mesh from the discriminator utility or MLP
    z = None

    if wghts is not None: # get the suface elevation z from weights and bias
      assert len(wghts) == len(WORLDS)
      w = wghts[i]
      if   w.shape[0] == 2: z = w[0]*x + w[1]*y # lin
      elif w.shape[0] == 3: z = w[0]*x + w[1]*y + w[2]*bias # lin + bias
      elif w.shape[0] == 4: z = w[0]*x + w[1]*y + w[2]*x*y + w[3]*bias # quad

    elif mlps is not None: # get the suface elevation z from the MLP output
      assert len(mlps) == len(WORLDS)
      mlp = mlps[i]
      z_mat = []
      for j in y_vec:
        z_row = []
        for k in x_vec:
          z_row.append(mlp.forward(np.array([j,k])))
        z_mat.append(z_row)
      z = np.squeeze(np.array(z_mat))

    assert x.shape == y.shape and y.shape == z.shape

    # Plot the surface
   #ax[i].plot_surface(x, y, z, color='lightgrey', alpha=0.5,
   #                   linewidth=0, antialiased=False)
    ax[i].plot_surface(x+0.5, y+0.5, z, color='lightgrey', alpha=0.5,
                       linewidth=0, antialiased=False)

    # White background
    x, y = np.meshgrid(np.arange(0, X+1, 1), np.arange(0, Y+1, 1))
    z = x*(-1.0)
    ax[i].plot_surface(x, y, z, color='white', alpha=0.01)

  plt.show()
  # Save figure
  fig.tight_layout()
  fig.savefig(filename, dpi=300) # , bbox_inches='tight')
  plt.close(fig)

#----------------------------------------------------------------#
# Plot the convergence of weights
# NB Convergence of weights doesn't guarantee quality of discrimination

def plot_errors(data, filepath="/tmp/plot_errors.png",
                x_label="X", y_label="Y", x_range=(0, 1), y_range=(0,1),
                color="-r", kernel_size=50, alpha=0.4, grid=True):
  num_data = len(data)
  assert num_data > 1
  fig = plt.figure()
  ax = fig.add_subplot(111, autoscale_on=False, xlim=x_range, ylim=y_range)
  ax.grid(grid)
  ax.set_xlabel(x_label)
  ax.set_ylabel(y_label)
  ax.plot(data, color, alpha=alpha) # rough graph in background
  kernel = np.ones(int(kernel_size))/float(kernel_size) # smoothing convolution
  bot = int(kernel_size/2.0)
  top = int(num_data-(kernel_size/2.0))
  data_arange = np.arange(num_data)[bot:top]
  data_convd = np.convolve(data, kernel, "same")[bot:top]
  ax.plot(data_arange, data_convd, color, alpha=1.0)
  plt.show()
  fig.savefig(filepath)
  fig.clear()
  plt.close(fig)

#================================================================#
if __name__ == "__main__":
  main()

# vim:set ts=2 sw=2 expandtab indentexpr=

