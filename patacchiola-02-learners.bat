if [ $# -ge 3 ]; then
  opts="-t$1 -s$2 -q$3"
  if [ $# -ge 4 ] && [ ! "${4%%[Vv]*}" = "$4" ]; then opts="$opts -V"; shift; fi
  shift; shift; shift; shift
  for i in 0 1 2 3 4 5 6 7; do
    echo "python3 patacchiola-02-learners.py $opts $i $*"
          python3 patacchiola-02-learners.py $opts $i $*
  done
else
  echo "Use: ${0##*/} <epochs> <episodes> <prints> [V|v(erbose)]"
fi

