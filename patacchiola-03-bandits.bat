if [ $# -ge 4 ]; then
  game=$1
  opts="-$game -t$2 -s$3 -q$4"
  if [ $# -ge 5 ] && [ ! "${5%%[vV]*}" = "$5" ]; then opts="$opts -V"; shift; fi
  shift; shift; shift; shift
  if [ "$game" == "B" ]; then # multi-armed bandit
    for i in "Random" "Softmax" "Greedy" "Random-mostly-greedy" "Softmax-mostly-greedy" "Random-ever-more-greedy" "Softmax-ever-more-greedy" "Boltzmann" "Bayes-Thompson"; do
      echo python3 patacchiola-03-bandits.py $opts -B -a $i $*
           python3 patacchiola-03-bandits.py $opts -B -a $i $*
    done
  elif [ "$game" == "C" ]; then # mountain-car bandits: non-learners
    for i in "Random" "Opportunist" "Swinger1-busy" "Swinger1-lazy" "Swinger2-busy" "Swinger2-lazy" "Sarsa-learner" "MonteCarlo-learner"; do
      echo python3 patacchiola-03-bandits.py $opts -C -a $i $*
           python3 patacchiola-03-bandits.py $opts -C -a $i $*
    done
  fi
else
  echo "Use: ${0##*/} B|C <epochs> <episodes> <prints> [(v)erbose]"
fi

