#!/usr/bin/env python

# MIT License
# Copyright (c) 2017 Massimiliano Patacchiola
#               https://mpatacchiola.github.io/blog/
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import numpy as np, sys
np.set_printoptions(precision=3)

#================================================================#

#----------------------------------------------------------------#
# Dissecting Reinforcement Learning-Part.1 | mpatacchiola's blog #
#----------------------------------------------------------------#

# Markov example
# s0 ---> s0 : 0.9  s0 ---> s1 : 0.1
# s1 ---> s0 : 0.5  s1 ---> s1 : 0.5

def markov_example():
  # Transition Matrix T
  # Note pre-multiplication style: s' = s.T or  s_j' = sum_i s_i T_ij
  # Hence, sum_j T_ij = 1 is the sum of the exit probabilities of s_i
  T = np.array([[0.90, 0.10],
                [0.50, 0.50]])

  # Obtaining T after 3 steps
  T_3 = np.linalg.matrix_power(T, 3)
  # Obtaining T after 50 steps
  T_50 = np.linalg.matrix_power(T, 50)
  # Obtaining T after 100 steps
  T_100 = np.linalg.matrix_power(T, 100)

  # Print the matrices
  print("T: " + str(T))
  print("T_3: " + str(T_3))
  print("T_50: " + str(T_50))
  print("T_100: " + str(T_100))

  # Initial distribution
  v = np.array([[1.0, 0.0]])

  # Print it
  print("v: " + str(v))
  print("v_1: " + str(np.dot(v,T)))
  print("v_3: " + str(np.dot(v,T_3)))
  print("v_50: " + str(np.dot(v,T_50)))
  print("v_100: " + str(np.dot(v,T_100)))

#================================================================#
# Robot in a 4 x 3 world
# Magic numbers 4, 3, 12 = 4*3 derive from here
# +---------+
# | . . . +1|   +1 = Target
# | . # . -1|   -1 = Trap,  # = Wall/Obstacle "at position (1,1)", but see below
# | R . . . |    R = Robot (bottom left)
# +---------+

#----------------------------------------------------------------#
def main(scenario=0):
  # Initial state vector. The agent starts bottom left
  v = np.array([[0.0, 0.0, 0.0, 0.0,
                 0.0, 0.0, 0.0, 0.0,
                 1.0, 0.0, 0.0, 0.0]])

  # Transition matrix loaded from file. (It is too big to write here)
  T = np.load("T.npy")

  # Utility vector
  u = np.array([[0.812, 0.868, 0.918,  1.0,
                 0.762,   0.0, 0.660, -1.0,
                 0.705, 0.655, 0.611,  0.388]])

  reward = -0.04 # reward for initial state
  gamma = 1.0 # discount factor

  # Bellman equation to find the utility of initial state (starting point)
  utility_v = get_state_utility(reward, gamma, u, v, T)
  print("Utility of starting point: %.3f"%utility_v)

  if scenario == 1:
    display_state_utility_calc(u, v, T)

  elif scenario == 2:
    w = generate_utility(100)
    #print(w)

  elif scenario == 3:
    generate_policy(100)

  elif scenario == 4:
    S = generate_transition_matrix()
    compare(S,T)
    utility_v = get_state_utility(reward, gamma, u, v, S)
    print("Utility of starting point: %.3f"%utility_v)

#----------------------------------------------------------------#
def get_state_utility(reward, gamma, u, v, T):
  """Return the state utility.
  @param reward for that state
  @param gamma discount factor
  @param u utility vector
  @param v state vector
  @param T transition matrix
  @return the utility of the state
  """
  action_array = np.zeros(4)
  for action in range(4):
    action_array[action] = np.sum(np.multiply(u, np.dot(v, T[:,:,action])))
  return reward + gamma * np.max(action_array)

def display_state_utility_calc(u, v, T):
  for action in range(4):
    print("Initial state:", v)
    print("Action:       ", action)
    print("State vector: ", np.dot(v, T[:,:,action]))
    print("Utility:      ", np.multiply(u, np.dot(v, T[:,:,action])))
    print("Total:        ", np.sum(np.multiply(u, np.dot(v, T[:,:,action]))))

#----------------------------------------------------------------#
# Utility generation
def generate_utility(max_iters=0): # utility iteration algorithm
  # Modify to suit
  gamma = 0.999 # future reward discount factor
  epsilon = 0.01 # stopping criteria small value

  # List containing the data for each iteation
  graph_list = list()

  # Transition matrix loaded from file. (It is too big to write here)
  T = np.load("T.npy")

  # Reward vector
  r = np.array([-0.04, -0.04, -0.04, +1.0,
                -0.04,   0.0, -0.04, -1.0,
                -0.04, -0.04, -0.04, -0.04])

  # Initial utility vector
  u = np.array([0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0])
  w = u.copy() # working copy

  i = 0 # iteration counter
  while True:
    i += 1
    delta = 0
    graph_list.append(u)
    for s in range(12):
      reward = r[s]
      v = np.zeros((1,12))
      v[0,s] = 1.0
      w[s] = get_state_utility(reward, gamma, u, v, T)
      delta = max(delta, np.abs(w[s] - u[s])) # Stopping criteria
    print(w)

    if delta < epsilon * (1 - gamma) / gamma \
       or (max_iters > 0 and i >= max_iters):
      break
    else:
      u = w.copy() # save work and continue

    if "q" == input(): sys.exit()

  print("=================== FINAL RESULT ==================")
  print("Iterations:", i)
  print("Delta:", delta)
  print("Gamma:", gamma)
  print("Epsilon:", epsilon)
  print("==================== Utilities ====================")
  print(u[0:4])
  print(u[4:8])
  print(u[8:12])
  print("===================================================")
  return w

#----------------------------------------------------------------#
# Policy generation
def evaluate_policy(r, gamma, u, T, p):
  """Return the policy utility.
  @param r reward vector
  @param gamma discount factor
  @param u utility vector
  @param T transition matrix
  @param p policy vector
  @return the utility vector u
  """
  for s in range(12):
    if not np.isnan(p[s]):
      v = np.zeros((1,12))
      v[0,s] = 1.0
      action = int(p[s])
      u[s] = r[s] + gamma * np.sum(np.multiply(u, np.dot(v, T[:,:,action])))
  return u

def optimise_action(u, v, T):
  """Return optimal action.
  Returns action maximising expected utility in state s according to T and u.
  @param u utility vector
  @param v starting vector
  @param T transition matrix
  @return expected action (int)
  """
  actions_array = np.zeros(4)
  for action in range(4):
   # Expected utility of taking action in state s, according to T and u.
   actions_array[action] = np.sum(np.multiply(u, np.dot(v, T[:,:,action])))
  return np.argmax(actions_array)

def print_policy(p, shape):
  """Printing utility.
  Print the policy actions using symbols:
  ^, v, <, >   up, down, left, right
  *   terminal states
  #   obstacles
  """
  i = 0
  policy_string = ""
  for row in range(shape[0]):
    for col in range(shape[1]):
      if   p[i] == -1:     policy_string += " *  "
      elif p[i] ==  0:     policy_string += " ^  "
      elif p[i] ==  1:     policy_string += " <  "
      elif p[i] ==  2:     policy_string += " v  "
      elif p[i] ==  3:     policy_string += " >  "
      elif np.isnan(p[i]): policy_string += " #  "
      else:                policy_string += " ?  " # something went wrong
      i += 1
    policy_string += '\n'
  print(policy_string[:-1])

def generate_policy(max_iters=0): # policy iteration algorithm
  # Modify to suit
  gamma = 0.999
  epsilon = 0.0001
  T = np.load("T.npy")
  # Generate the first policy randomly
  # NaN=Nothing, -1=Terminal, 0=Up, 1=Left, 2=Down, 3=Right
  p = np.random.randint(0, 4, size=(12)).astype(np.float32)
  p[5] = np.NaN
  p[3] = p[7] = -1
  # Utility vectors
  u = np.array([0.0, 0.0, 0.0,  0.0,
                0.0, 0.0, 0.0,  0.0,
                0.0, 0.0, 0.0,  0.0])
  # Reward vector
  r = np.array([-0.04, -0.04, -0.04, +1.0,
                -0.04,   0.0, -0.04, -1.0,
                -0.04, -0.04, -0.04, -0.04])

  i = 0
  while True:
    i += 1
    # Step 1: Policy evaluation
    w = u.copy()
    u = evaluate_policy(r, gamma, u, T, p)
    # Stopping criteria
    delta = np.absolute(u - w).max()
    if delta < epsilon * (1 - gamma) / gamma \
       or max_iters > 0 and i >= max_iters: break
    for s in range(12):
      if not np.isnan(p[s]) and not p[s]==-1:
        v = np.zeros((1,12))
        v[0,s] = 1.0
        # Step 2: Policy improvement
        a = optimise_action(u, v, T)
        if a != p[s]: p[s] = a
    print(i);
    print_policy(p, shape=(3,4))
    if "q" == input(): sys.exit()

  print("=================== FINAL RESULT ==================")
  print("Iterations:", i)
  print("Delta:", delta)
  print("Gamma:", gamma)
  print("Epsilon:", epsilon)
  print("==================== Utilities ====================")
  print(u[0:4])
  print(u[4:8])
  print(u[8:12])
  print("==================== Policies =====================")
  print_policy(p, shape=(3,4))
  print("===================================================")

#================================================================#
# Generating the transition matrix:

# Try and use coordinate style position encoding consistent with state vector
# representation

# Position encoding:

# Initial position of R represented by: v = (0 0 0 0 0 0 0 0 1 0 0 0)
# Referred to as representing "position (1,1)" (but see above)
# Implying one based matrix style indexing (counting rows either way)

# Inspection of filed transition matrix T shows:
# One up from initial position is given by: (0 0 0 0 1 0 0 0 0 0 0 0)
# One right from initial position given by: (0 0 0 0 0 0 0 0 0 1 0 0)

# Hence the following choices:

def position_code(x,y):
# code = 3*x+y # rows from origin (bottom) / columns from origin (left)
# code = x+4*y # columns from origin (left) / rows from origin (bottom)
# code = 3*x+2-y # rows from top / columns from origin (left)
  code = x+4*(2-y) # columns from origin (left) / rows from top
  return code

def next_position_codes(x,y):
  curr  = position_code(x,y)
  up    = position_code(x,  y+1)   # x' = x,   y' = y+1
  left  = position_code(x-1,y  )   # x' = x-1, y' = y
  down  = position_code(x,  y-1)   # x' = x,   y' = y-1
  right = position_code(x+1,y  )   # x' = x+1, y' = y
  return curr, up, left, down, right

def display_row(T,x,y,action):
  curr, up, left, down, right = next_position_codes(x,y)
  print(x,y,"|",action,"|",end=" ")
  print("%.2f"%T[curr,curr,action],"|",end=" ")
  print("%.2f"%T[curr,up,action],end=" ")   if y < 2 else print("    ",end=" ")
  print("%.2f"%T[curr,left,action],end=" ") if x > 0 else print("    ",end=" ")
  print("%.2f"%T[curr,down,action],end=" ") if y > 0 else print("    ",end=" ")
  print("%.2f"%T[curr,right,action])        if x < 3 else print("    ")
  # if "q" == input(): sys.exit()

def generate_transition_matrix(docile=True):
  # Rules may differ from original:
  # Wild  : Try all four alternatives, forward with 0.7, rest with 0.1
  # Docile: Omit reversing, forward with 0.8, deviate left/right with 0.1
  # NB: Action codes in part 1 may differ from those used in subsequent
  major = 0.7
  minor = 0.1
  T = np.zeros((12,12,4)) # T[,:,:a] = [T_{from}_{to}] for action a
  for action in range(4): # Actions: Up=0 Left=1 Down=2 Right=3
    # Starting state s ~ (x,y) ending states s' ~ (x',y')
    for x in range(4):
      for y in range(3):
        if x == 1 and y == 1: continue
        # display_row(T,x,y,action)
        curr, up, left, down, right = next_position_codes(x,y)
        # Bounce off wall or obstacle to remain in current position
        if y == 2 or (x   == 1 and y+1 == 1): up    = curr
        if x == 0 or (x-1 == 1 and y   == 1): left  = curr
        if y == 0 or (x   == 1 and y-1 == 1): down  = curr
        if x == 3 or (x+1 == 1 and y   == 1): right = curr
        if action == 0: # Up
          if docile: down = up # Add reverse probability to forward
          T[curr, up, action] += major
          T[curr, left, action] += minor
          T[curr, down, action] += minor
          T[curr, right, action] += minor
        elif action == 1: # Left
          if docile: right = left # Ditto above
          T[curr, up, action] += minor
          T[curr, left, action] += major
          T[curr, down, action] += minor
          T[curr, right, action] += minor
        elif action == 2: # Down
          if docile: up = down # ...
          T[curr, up, action] += minor
          T[curr, left, action] += minor
          T[curr, down, action] += major
          T[curr, right, action] += minor
        elif action == 3: # Right
          if docile: left = right # ...
          T[curr, up, action] += minor
          T[curr, left, action] += minor
          T[curr, down, action] += minor
          T[curr, right, action] += major
        display_row(T,x,y,action)
  return T

def compare(A,B):
  for action in range(4):
    print(A[:,:,action])
    print(B[:,:,action])
    if "q" == input(str(action) + ":"): sys.exit()

#================================================================#
def usage(x):
  print("Use:", x, "0|1|2|3|4 in order to run a scenario")
  sys.exit()

#----------------------------------------------------------------#
if __name__ == "__main__":
  scenario = 0;
  if len(sys.argv) > 1:
    try:
      scenario = int(sys.argv[1])
    except:
      usage(sys.argv[0])
  if scenario >= 0 and scenario < 5:
    main(scenario)
  else:
    usage(sys.argv[0])
#================================================================#

# vim:set ts=2 sw=2 expandtab indentexpr=
