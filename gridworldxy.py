#!/usr/bin/env python

# MIT License
# Copyright (c) 2017 Massimiliano Patacchiola
#               https://mpatacchiola.github.io/blog/
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""
Class for creating a gridworld of arbitrary size and with arbitrarily placed
obstacles.
The state of the agent in the gridworld is its position. The state matrix
determines which positions are available (0), terminal (1) or unreachable (-1).
With each such state of the gridworld is associated a reward.
The transition matrix defines the probabilities of executing an action given
an instruction to the robot.
Action codes are: 0=UP, 1=RIGHT, 2=DOWN, 3=LEFT. (Note left/right swap from
part 1 of "Dissecting Reinforcement Learning")
Coordinates x/y are as for geometric patches, e.g., in the "Cleaning Robots 5x5"world:

 (0,2) (1,2) (2,2) (3,2)
 (0,1) (1,1) (2,1) (3,1)
 (0,0) (1,0) (2,0) (3,0)

When storing a patch in a NumPy array the corresponding row/column indices
become:

 (0,0) (0,1) (0,2) (0,3)
 (1,0) (1,1) (1,2) (1,3)
 (2,0) (2,1) (2,2) (2,3)

One could choose to do back-and-forth acrobatics all over the place, be it to
re-calculate UP/DOWN, LEFT/RIGHT plus boundary checks in indices, or conversely
calculate x or y coordinates from (ad-hoc encodings of) rows and columns...
"""

import numpy as np
np.set_printoptions(precision=3)

class GridWorldxy:

  Up, Right, Down, Left = 0, 1, 2, 3 # action codes
  actions = [ Up, Right, Down, Left ]
  num_acts = len(actions) # number of actions
  Stop, NoGo = num_acts, -1 # non-action codes
  Free, Exit, Wall = 0, 1, -1 # state/position codes

  def __init__(self, X, Y):
    self.X = X # width of the grid-world (horizontal axis going right)
    self.Y = Y # height of the grid-world (vertical axis going up)
    self.Transitions = np.ones((self.num_acts, self.num_acts)) / self.num_acts
    self.Rewards = np.zeros((X, Y)) # reward matrix
    self.States = np.zeros((X, Y)) # state matrix
    self.pos = [np.random.randint(X), np.random.randint(X)] # initial position of robot

  def setTransitionMatrix(self, Transitions):
    """Set the transition matrix.

    In the transition matrix each row lists the probabilities of the actions
    specified in the columns under the policy specified for the row. Example:

           UP     RIGHT LEFT  DOWN  <<<<< actions taken
    UP    [[0.55, 0.25, 0.10, 0.10]
    RIGHT  [0.25, 0.25, 0.25, 0.25]
    DOWN   [0.30, 0.20, 0.40, 0.10]
    LEFT   [0.10, 0.20, 0.10, 0.60]]
    ^^^^^
    actions mandated by policies

    The first row lists the probabilities of executing one of the 4 actions
    when the policy is UP. With a probability of 0.55 the robot will go UP,
    with a probability of 0.25 RIGHT, with 0.10 DOWN or LEFT, respectively.
    """
    assert Transitions.shape == self.Transitions.shape
    self.Transitions = Transitions

  def setRewardMatrix(self, Rewards):
    """Set the reward matrix.

    """
    assert Rewards.shape == self.Rewards.shape
    self.Rewards = Rewards

  def setStateMatrix(self, States):
    """Set the obstacles in the world.

    The input to the function is a matrix with the same size of the world
      -1 for positions which are forbidden (walls)
      +1 for terminal positions (exits)
       0 for all the walkable (free) positions
    Example: States of the 4x3 world from "Dissecting Reinforcement Learning"
      0,  0,  0, +1
      0, -1,  0, +1
      0,  0,  0,  0
    """
    assert States.shape == self.States.shape
    self.States = States

  def setPosition(self, x=None, y=None):
    """ Set the position of the robot in a specific state.

    """
    if x is None or y is None:
      self.pos = [np.random.randint(self.X), np.random.randint(self.Y)]
    else:
      self.pos = [x, y]

  def render(self):
    """ Print the current world in the terminal.

    O represents the robot position
    - respresent empty states.
    # represents obstacles
    * represents terminal states
    """
    graph = ""
    for y in range(self.Y):
      for x in range(self.X):
        if self.pos == [x, self.Y-1-y]:
          # graph += u" \u25CB " # u" \u25CC "
          graph += " R "
        else:
          if   self.States[x, self.Y-1-y] ==  0: graph += " - "
          elif self.States[x, self.Y-1-y] == -1: graph += " # "
          elif self.States[x, self.Y-1-y] == +1: graph += " * "
      graph += '\n'
    print(graph[:-1])

  def reset(self, exploring_starts=False):
    """ Set the position of the robot in the bottom left corner.

    It returns the first observation
    """
    if exploring_starts:
      while True:
        x = np.random.randint(0, self.X)
        y = np.random.randint(0, self.Y)
        if self.States[x, y] == 0: break
      self.pos = [x, y]
    else:
      self.pos = [0, 0]
    return self.pos

  def step(self, action):
    """ One step in the world.

    [observation, reward, done = env.step(action)]
    The robot moves one step in the world based on the action given.
    @return observation the position of the robot after the step
    @return reward the reward associated with the next state
    @return done True if the state is terminal
    """
    assert action in self.actions

    # Given mandated action select action to perform via the transition model
    # actual action = "Transition model(mandated action)"
    action = np.random.choice(4, 1, p=self.Transitions[int(action),:])

    # Generating a new position based on the current position and action
    x, y = new_pos = self.pos
    if   action == self.Up:    new_pos = [x  , y+1]
    elif action == self.Right: new_pos = [x+1, y  ]
    elif action == self.Down:  new_pos = [x,   y-1]
    elif action == self.Left:  new_pos = [x-1, y  ]

    # Check if the new position is a valid position
    u,v = new_pos
    reward = 0
    done = False
    if u >= 0 and u < self.X and v >= 0 and v < self.Y \
                     and self.States[u, v] != self.Wall:
      self.pos = new_pos
      reward = self.Rewards[u, v]
      done = bool(self.States[u, v] == self.Exit) # done if state is terminal
    return self.pos, reward, done

  """Back and forth between geometric coordinates and matrix indices"""
  @staticmethod
  def rotate090(M): # matrix to patch: flip across diagonal and flop over mid horizontal
    Y, X = M.shape
    P = np.zeros((X,Y))
    for y in range(Y):
      for x in range(X):
        P[x,y] = M[Y-y-1,x]
    return P

  @staticmethod
  def rotate270(P): # patch to matrix: reverse
    X, Y = P.shape
    M = np.zeros((Y,X))
    for y in range(Y):
      for x in range(X):
        M[Y-y-1,x] = P[x,y]
    return M

  @classmethod
  def matrix_to_patch(cls, M): return cls.rotate090(M)

  @classmethod
  def patch_to_matrix(cls, P): return cls.rotate090(P)

  @classmethod
  def print_patch(cls, P): print(cls.rotate270(P))

  @classmethod
  def nm_to_xy(cls, M): return cls.matrix_to_patch(M) # some shorthands

  @classmethod
  def xy_to_nm(cls, P): return cls.patch_to_matrix(P) # ...

  @classmethod
  def print_xy(cls, P): cls.print_patch(P) # ...

  @classmethod
  def desc_action(cls, a):
    s = "N/A"
    if   a == cls.Up    : s = "UP"
    elif a == cls.Right : s = "RIGHT"
    elif a == cls.Down  : s = "DOWN"
    elif a == cls.Left  : s = "LEFT"
    elif a == cls.Stop  : s = "STOP"
    return s

  @classmethod
  def pict_action(cls, a):
    s = "#"
    if   a == cls.Up    : s = "^"
    elif a == cls.Right : s = ">"
    elif a == cls.Down  : s = "v"
    elif a == cls.Left  : s = "<"
    elif a == cls.Stop  : s = "o"
    return s

  @classmethod
  def desc_act(cls, a): return cls.desc_action(a)

  @classmethod
  def pict_act(cls, a): return cls.pict_action(a)

  @classmethod
  def print_actions_patch(cls, P):
    X, Y = P.shape
    for y in range(Y):
      for x in range(X):
        print("%3s" % cls.pict_act(P[x, Y-1-y]), end="")
      print()

  @classmethod
  def print_actions_xy(cls, P): cls.print_actions_patch(P)

# vim:set ts=2 sw=2 expandtab indentexpr=
