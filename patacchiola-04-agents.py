#!/usr/bin/env python

# MIT License
# Copyright (c) 2017 Massimiliano Patacchiola
#               https://mpatacchiola.github.io/blog/
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import getopt, math, numpy as np, os, random, sys
np.set_printoptions(precision=3)

from termcolor import colored

verbose = False
display = False
datasource = None

#================================================================#
# Dissecting Reinforcement Learning-Part.6 | mpatacchiola’s blog #
#================================================================#

#================================================================#

def main():
  global verbose, display, datasource
  games = ["Mountain-car", "Cart-pole"]
  game = games[0]
  agents = ["Random-agent", "SARSA-learner", "MonteCarlo-learner"]
  agent = agents[0]

  # Execution params central ...
  max_epochs = 5000 # number of learning cycles
  max_episodes = 500 # length of learning cycle
  freeze_delay = 1 # heat decay attenuation (small = freeze fast, large = slow)
  max_quotes = 50 # total number of stats print-outs desired
  num_x_cells = 21; num_v_cells = 11 # granularity of discretisations
  delta = 0.1 # time step-size

  mass = 0.2; friction = 0.3 # Mountain-car
  pole_mass = 2.0; cart_mass = 8.0; pole_len = 0.5 # Cart-pole

  # Set all your params here ...
  opts, args = getopt.getopt(sys.argv[1:],
    "g:a:n:t:s:r:q:d:VW", [ # execution params
      "game=", "agent=", # basic params, the only ones to be duplicated here
      "num_x_cells=", "num_v_cells=", "delta=", # discretisation params
      "mass=", "friction=", # mountain-car params
      "pole_mass=", "pole_len=", "cart_mass=", # cart-pole params
    ]
  )

  for opt in opts:
    if   opt[0] == "-g": game = games[int(opt[1])]
    elif opt[0] == "-a": agent = agents[int(opt[1])]
    elif opt[0] == "-n": num_x_cells, num_v_cells = map(int, opt[1].split("x"))
    elif opt[0] == "-t": max_epochs = int(opt[1])
    elif opt[0] == "-s": max_episodes = int(opt[1])
    elif opt[0] == "-r": freeze_delay = int(opt[1])
    elif opt[0] == "-q": max_quotes = int(opt[1])
    elif opt[0] == "-d": datasource = opt[1]
    elif opt[0] == "-V": verbose = True
    elif opt[0] == "-W": display = True
    elif opt[0] == "--game": game = opt[1]
    elif opt[0] == "--agent": agent = opt[1]
    elif opt[0] == "--num_x_cells": num_x_cells = int(opt[1])
    elif opt[0] == "--num_v_cells": num_v_cells = int(opt[1])
    elif opt[0] == "--delta": delta = float(opt[1])
    elif opt[0] == "--mass": mass = float(opt[1])
    elif opt[0] == "--friction": friction = float(opt[1])
    elif opt[0] == "--pole-mass": pole_mass = float(opt[1])
    elif opt[0] == "--pole-len": pole_len = float(opt[1])
    elif opt[0] == "--cart-mass": cart_mass = float(opt[1])

  if max_epochs < 1 or max_episodes < 1 or max_quotes < 0:
    print("Check your params")
    sys.exit()

  if max_quotes > max_epochs: max_quotes = max_epochs

  # Then run the agent ...
  if game == "Mountain-car":
    run_mountain_car(mass, friction, delta, \
      agent, max_epochs, max_episodes, freeze_delay, max_quotes, \
      num_x_cells, num_v_cells)
  elif game == "Cart-pole":
    run_cart_pole(pole_mass, pole_len, cart_mass, delta,
      agent, max_epochs, max_episodes, freeze_delay, max_quotes, \
      num_x_cells, num_v_cells)

#================================================================#
# Agents

#----------------------------------------------------------------#
# SARSA learner
def sarsa_learner(env, strat, new_obs, epoch, stage):

  if epoch == 0 and stage == 0:
    print("Initial policy matrix")
    strat.print_policy()

  if stage == 0:
    strat.apply_cooling(epoch)
    print("Epoch", epoch, "starting at", "%.3f,%.3f"%(new_obs[0],new_obs[1]), "...")

  # Save last observation
  cur_obs = new_obs
  # Convert from floating values to cell IDs
  cur_cells = env.discretise(cur_obs)
  # Select a move according to the policies
  cur_move = strat.select_move_from_policy(cur_cells)
  # At the start of an epoch allow for exploratory random moves
  if stage == 0: cur_move = np.random.randint(env.LEFT, env.RIGHT)
  # Execute, and measure environment reaction (get new pos and vel)
  new_obs, reward, done = env.step(cur_move)
  # Convert floating values to cell IDs
  new_xc, new_vc = new_cells = env.discretise(new_obs)
  # Move one step in the environment, get next observation and reward
  new_move = strat.policy[new_xc, new_vc]
  # Update utility from move mandated for new state
  strat.update_utility(cur_cells, new_cells, cur_move, new_move, reward)
  # Increment visit count
  strat.update_visits(cur_cells, cur_move)
  # Update policy from utility
  strat.update_policy(cur_cells)

  return cur_obs, cur_move, new_obs, reward, done, strat.epsv[1] # for stats

#----------------------------------------------------------------#
# Monte Carlo Learner
def monte_carlo_learner(env, strat, new_obs, epoch, stage):

  if epoch == 0 and stage == 0:
    print("Initial policy matrix")
    strat.print_policy()

  if stage == 0:
    print("Epoch", epoch, "starting at", "%.3f,%.3f"%(new_obs[0],new_obs[1]), "...")
    strat.apply_cooling(epoch)
    strat.episodes = []

  # Save last observation
  cur_obs = new_obs
  # Convert from floating values to cell IDs
  cur_cells = env.discretise(cur_obs)
  # Select a move according to the policies
  cur_move = strat.select_move_from_policy(cur_cells)
  # If the epoch just started then choose a random action (exploring starts)
  if stage == 0: cur_move = np.random.randint(env.LEFT, env.RIGHT)
  # Move one step in the environment, get next observation and reward
  new_obs, reward, done = env.step(cur_move)
  # Convert floating values to cell IDs
  new_cells = env.discretise(new_obs)
  # Record the current action with the new observation
  strat.episodes.append((cur_move, new_cells, reward))

  if done or stage == strat.max_episodes -1:
    # Epoch finished, calculate utilities implementing First-Visit MC algo:
    # For each recorded state, if encountered for the first time in the stream,
    # estimate its expected return and update utilities
    already_visited = np.zeros((env.num_moves, env.N_x, env.M_v))
    for k in range(len(strat.episodes)):
      stage = strat.episodes[k]
      move = stage[0]
      cells = stage[1]
      x_cell, v_cell = cells
      if already_visited[move, x_cell, v_cell] == 0: # not seen before
        already_visited[move, x_cell, v_cell] = 1 # mark as seen
        Return = strat.expected_return(strat.episodes[k:]) # get Return
        strat.utility[move, x_cell, v_cell] += Return # update utility
        strat.visits[move, x_cell, v_cell] += 1 # update visit counts
        # Update policy from (frequency discounted) utility
        # (Only done at most once per cell pair visited)
        strat.update_policy(cells)

  return cur_obs, cur_move, new_obs, reward, done, strat.epsv[1] # for stats

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
# Mountain car non-learners

# Full swinger
def swinger(env, cur_obs, cur_move, new_obs):
  new_move = np.random.randint(env.LEFT,env.RIGHT)
  if cur_obs is None or new_obs is None or cur_move is None:
    if random.randint(0,1) == 0: new_move = env.LEFT
  else:
    new_move = cur_move
    x_cur, v_cur = cur_obs
    x_new, v_new = new_obs
    if (x_new < x_cur and cur_move == env.RIGHT) \
    or (x_new > x_cur and cur_move == env.LEFT ): new_move = -cur_move
  cur_obs = new_obs
  cur_move = new_move
  new_obs, reward, done = env.step(cur_move)
  return cur_obs, cur_move, new_obs, reward, done

# Busy/lazy swinger (push uphill/downhill only)
def partial_swinger1(env, cur_obs, cur_move, new_obs, mode="+"):
  x_new, v_new = new_obs
  y_new = env.elevation(x_new)
  new_move = np.random.randint(env.LEFT,env.RIGHT)
  if cur_obs is not None:
    x_cur, v_cur = cur_obs
    y_cur = env.elevation(x_cur)
    if x_new < x_cur:
      if   mode == "+" and y_new > y_cur: new_move = env.LEFT
      elif mode == "-" and y_new < y_cur: new_move = env.LEFT
    elif x_new > x_cur:
      if   mode == "+" and y_new > y_cur: new_move = env.RIGHT
      elif mode == "-" and y_new < y_cur: new_move = env.RIGHT
    elif new_move == env.DRIFT and x_new == x_cur:
      if cur_move is None: new_move = np.random.randint(env.LEFT,env.RIGHT)
      else:                new_move = -cur_move
  cur_obs = new_obs
  cur_move = new_move
  new_obs, reward, done = env.step(cur_move)
  return cur_obs, cur_move, new_obs, reward, done

# Variant of preceding
def partial_swinger2(env, cur_obs, cur_move, new_obs, mode="+"):
  x_new, v_new = new_obs
  y_new = env.elevation(x_new)
  new_move = np.random.randint(env.LEFT,env.RIGHT)
  if cur_obs is not None:
    x_cur, v_cur = cur_obs
    y_cur = env.elevation(x_cur)
    if (mode == "+" and y_new > y_cur) or (mode == "-" and y_new < y_cur):
      if   x_new < x_cur: new_move = env.LEFT
      elif x_new > x_cur: new_move = env.RIGHT
    if new_move == env.DRIFT and x_new == x_cur:
      if cur_move is None: new_move = np.random.randint(env.LEFT,env.RIGHT)
      else:                new_move = -cur_move
  cur_obs = new_obs
  cur_move = new_move
  new_obs, reward, done = env.step(cur_move)
  return cur_obs, cur_move, new_obs, reward, done

#================================================================#
# Agent runners

#----------------------------------------------------------------#
# Mountain car driver
def run_mountain_car(mass, friction, delta, \
  agent, max_epochs, max_episodes, freeze_delay, max_quotes, \
  num_x_cells, num_v_cells):

  env = MountainCar(mass, friction, delta)
  strat = Strategy(max_epochs, max_episodes, freeze_delay, max_quotes, \
                   num_x_cells, num_v_cells)


  eps = 0 # heat param
  target = 1.0 # exit point

  if agent[-7:] == "-learner":
    if datasource is not None:
      strat.policy = strat.load_policy(datasource)
      strat.epsv = [0.0, 0.0, 0.0]

  quit = halt = False

  print("Mountain Car (" + agent + ") running")
  if display:
    screen, font, clock \
      = launch_display("Mountain Car (" + agent + ")")

  for epoch in range(max_epochs):

    print("Epoch", epoch)
    # Reset and return the first observation
    new_obs = env.reset(exploring_starts=True)
    cur_obs = cur_move = new_move = None

    benchmark = 0.0 # high watermark on approach to exit (the larger the better)
    reward = tally = 0
    done = False

    for stage in range(max_episodes):

      # Calculus
      if done:
        pass

      elif agent == "Random-agent":
        cur_obs = new_obs
        new_move = random.randint(env.LEFT, env.RIGHT)
        cur_move = new_move
        new_obs, reward, done = env.step(cur_move)

      elif agent == "Swinger":
        cur_obs, cur_move, new_obs, reward, done \
          = swinger(env, cur_obs, cur_move, new_obs)

      elif agent == "Uphill-swinger":
        cur_obs, cur_move, new_obs, reward, done \
          = partial_swinger1(env, cur_obs, cur_move, new_obs, mode="+")

      elif agent == "Downhill-swinger":
        cur_obs, cur_move, new_obs, reward, done \
          = partial_swinger1(env, cur_obs, cur_move, new_obs, mode="-")

      elif agent == "SARSA-learner":
        cur_obs, cur_move, new_obs, reward, done, eps \
          = sarsa_learner(env, strat, new_obs, epoch, stage)

      elif agent == "MonteCarlo-learner":
        cur_obs, cur_move, new_obs, reward, done, eps \
          = monte_carlo_learner(env, strat, new_obs, epoch, stage)

      else:
        print("No such agent:", agent)
        sys.exit()

      tally += reward
      if new_obs[0] > benchmark: benchmark = new_obs[0]

      if display:
        quit, halt = paint_mountain_car(env, cur_obs, cur_move, new_obs, \
          epoch, stage, screen, font, clock, halt, reward, tally)

      if done or quit: break
      elif halt:       input("...")

    epoch_closing_quote(strat, epoch, stage, tally, done, eps, target, benchmark)

    if quit: break

  terminal_quote(tally)

  if display:
    time.sleep(3)
    pygame.quit()

#----------------------------------------------------------------#
# Pole cart driver
def run_cart_pole(pole_mass, pole_len, cart_mass, delta,
  agent, max_epochs, max_episodes, freeze_delay, max_quotes, \
  num_x_cells, num_v_cells):

  env = CartPole(pole_mass, cart_mass, pole_len, delta)
  strat = Strategy(max_epochs, max_episodes, freeze_delay, max_quotes, \
                   num_x_cells, num_v_cells)

  eps = 0 # heat param
  target = 0.0 # straight up

  if agent[:-7] == "-learner":
    if datasource is not None:
      strat.policy = strat.load_policy(datasource)
      strat.epsv = [0.0, 0.0, 0.0]

  quit = halt = False

  print("Cart Pole (" + agent + ") running")
  if display:
    screen, font, clock \
      = launch_display("Cart pole (" + agent + ")")

  for epoch in range(max_epochs):

    print("Epoch", epoch)
    # Reset and return the first observation
    new_obs = env.reset(exploring_starts=True)
    cur_obs = cur_move = new_move = None

    benchmark = 0.0 # max divergence from vertical (the smaller the better)
    reward = tally = 0
    done = False

    for stage in range(max_episodes):

      # Calculus
      if done:
        pass

      elif agent == "Random-agent":
        cur_obs = new_obs
        new_move = random.randint(env.LEFT, env.RIGHT)
        cur_move = new_move
        new_obs, reward, done = env.step(cur_move)

      elif agent == "Balancer":
        cur_obs = new_obs
        new_move = env.DRIFT
        if new_obs[0] > 0: new_move = env.LEFT
        elif new_obs[0] < 0: new_move = env.RIGHT
        cur_move = new_move
        new_obs, reward, done = env.step(cur_move)

      elif agent == "SARSA-learner":
        cur_obs, cur_move, new_obs, reward, done, eps \
          = sarsa_learner(env, strat, new_obs, epoch, stage)

      elif agent == "MonteCarlo-learner":
        cur_obs, cur_move, new_obs, reward, done, eps \
          = monte_carlo_learner(env, strat, new_obs, epoch, stage)

      else:
        print("No such agent:", agent)
        sys.exit()

      tally += reward
      if abs(new_obs[0]) > benchmark: benchmark = abs(new_obs[0])

      if display:
        quit, halt = paint_pole_cart(env, cur_obs, cur_move, new_obs, \
          epoch, stage, screen, font, clock, halt, reward, tally)

      if done or quit: break
      elif halt:       input("...")

    epoch_closing_quote(strat, epoch, stage, tally, done, eps, target, benchmark)

    if quit: break

  terminal_quote(tally)

  time.sleep(3)
  pygame.quit()

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
# Some output routines

#----------------------------------------------------------------#
def terminal_quote(tally):
  print("[ %.3f @"%tally, \
        time.strftime("%Y-%m-%d %H:%M:%S]", time.localtime()))

#----------------------------------------------------------------#
def epoch_closing_quote(strat, epoch, stage, tally, done, eps, goal, mark):
  need_quote = (epoch == strat.max_epochs-1 \
    or (strat.max_quotes > 0 and epoch%strat.print_period == 0))

  if verbose or need_quote:
    c = "green" if abs(mark - goal) < 0.01 else "red"
    print("Policies after", stage+1, "episodes,", \
          "eps: %.3f ["%eps, colored("%.3f"%mark,c), "] tally: %.3f"%tally, \
          ("[ done ]" if done else ""))
    strat.print_policy()

#================================================================#
class Generics:
  #-------------------------------------------------------------#
  # Eternals
  #pi = 3.14159265358979323844 # print(os.popen("echo \"4*a(1)\"|bc -l").read())
  gamma = 0.999 # future discount factor (rewards further ahead count less)

  # Moves represented by integers, possibly negative ...
  LEFT = -1
  DRIFT = 0 # or NONE? But "no move" doesn't have to mean standstill...
  RIGHT = 1
  moves = [ LEFT, DRIFT, RIGHT ]
  num_moves = len(moves)

  # The index of 'LEFT' is 0, etc: Routines to rebase at 0 a contiguous integer
  # list of "moves" ...
  def index_of_move(self, m):
    index = int(m - self.moves[0])
    if verbose: assert index >= 0
    return index
  # ... or unrebase from
  def move_of_index(self, i):
    move = (i + self.moves[0])
    if verbose: assert move in self.moves
    return move

  # Omitted from object for transparency
  # cur_move = new_move = None # current and next move
  # cur_obs = new_obs = None # current and new environment responses

  #------------------------------------------------------------#
  # Overridable dynamics params with dummy values
  # Positions on an axis or angles on a cicle
  x_min = -1 # boundary left
  x_max =  1 # boundary right
  x_init = 0 # starting point
  # Velocities, linear or angular
  v_min = -1 # boundary left
  v_max =  1 # boundary right
  v_init = 0 # starting speed

  #------------------------------------------------------------#
  # Discretisation, via superimposed grid (of "blocks", "cells", or "bins" ...)
  # Specify either size of cells, or their number, and deduce the other, e.g.,

  size_x_cell = size_v_cell = 0.1 # dummy value
  N_x = num_x_cells = 10 # N_x is a short-hand alias for num_x_cells # dummies
  M_v = num_v_cells = 10 # M_v is a short-hand alias for num_v_cells # dummies

  # Omitted from object for transparency
  # cur_cells = new_cells = None # current/subsequent (pos/vel) cells occupied

  def set_num_x_cells(self):
    self.N_x = self.num_x_cells \
      = int(math.ceil((self.x_max - self.x_min) / self.size_x_cell))
  def set_num_v_cells(self):
    self.M_v = self.num_v_cells \
      = int(math.ceil((self.v_max - self.v_min) / self.size_v_cell))

  def set_size_x_cell(self):
    self.size_x_cell = (self.x_max - self.x_min) / self.num_x_cells
  def set_size_v_cell(self):
    self.size_v_cell = (self.v_max - self.v_min) / self.num_v_cells

  # Pedestrian routines to convert between floats and cell IDs

  # Associate to cell ID n the left end-point z of cell n
  # (not needed, just to exhibit the rule)
  def x_cell_to_coord(self, n):
    return self.x_min + n * self.size_x_cell
  def v_cell_to_coord(self, n):
    return self.v_min + n * self.size_v_cell

  # Associate to floating point z the ID n of the cell containing it
  # (needed, obtained by inverting preceding)
  def x_coord_to_cell(self, z):
    return int(math.floor((z - self.x_min) / self.size_x_cell))
  def v_coord_to_cell(self, z):
    return int(math.floor((z - self.v_min) / self.size_v_cell))

  # Create cells with NumPy
  # x_cells = v_cells = np.linspace(0, 1, 10) # dummy example
  def set_x_cells(self):
    self.x_cells \
      = np.linspace(self.x_min, self.x_max, num=self.N_x, endpoint=False)
  def set_v_cells(self):
    self.v_cells \
      = np.linspace(self.v_min, self.v_max, num=self.M_v, endpoint=False)

  # Meta routine converting coordinate floats to cell IDs
  def discretise(self, obs):
    x, v = obs
    # Via leg-work
    cells = [self.x_coord_to_cell(x), self.v_coord_to_cell(v)]
    # Or via x_cells/v_cells
    #cells = (np.digitize(x, self.x_cells), np.digitize(v, self.v_cells))
    if verbose:
      if cells[0] < 0 or cells[0] >= self.num_x_cells:
        print("Illegal cell", cells[0], "for x=", x)
      if cells[1] < 0 or cells[1] >= self.num_v_cells:
        print("Illegal cell", cells[1], "for v=", v)
    if cells[0] < 0: cells[0] = 0
    elif cells[0] >= self.num_x_cells: cells[0] = self.num_x_cells-1
    if cells[1] < 0: cells[1] = 0
    elif cells[1] >= self.num_v_cells: cells[1] = self.num_v_cells-1
    return cells

  #------------------------------------------------------------#
  # Constructor
  def __init__(self, max_epochs, max_episodes, freeze_delay, max_quotes, \
    num_x_cells, num_v_cells):

    # Set learning hyper params
    self.max_epochs = max_epochs
    self.max_episodes = max_episodes
    self.freeze_delay = freeze_delay
    self.max_quotes = max_quotes

    self.print_period = self.max_epochs//self.max_quotes

    # Grid maintenance
    self.N_x = self.num_x_cells = num_x_cells
    self.set_size_x_cell()
    self.set_x_cells()
    self.M_v = self.num_v_cells = num_v_cells
    self.set_size_v_cell()
    self.set_v_cells()

    print("Generics constructed with params", \
      self.max_epochs, self.max_episodes, self.freeze_delay, self.max_quotes, \
      self.num_x_cells, self.N_x, self.num_v_cells, self.M_v)

#================================================================#
class Strategy(Generics):
  #------------------------------------------------------------#
  # Policy, utility and usage count matrices for states or action/state pairs
  # Position/velocity states are defined for cells in the grid, so dimensions
  # are, variously, ( num_moves * ) num_x_cells * num_v_cells; e.g.,

  # policy = np.zeros(shape=((N_x, M_v))) # dummy value
  # utility = np.zeros(shape=((num_moves, N_x, M_v)))
  # visits = np.zeros(shape=((num_moves, N_x, M_v)))

  N_x = Generics.N_x
  M_v = Generics.M_v

  # Policy recommends a move (LEFT, NONE, RIGHT) for each state (pos, vel)
  def randomise_policies(self): # initially random
   #self.policy = np.zeros(shape=((self.N_x, self.M_v))) # dummy value
    self.policy \
      = np.random.randint(low=int(self.LEFT), high=int(self.RIGHT)+1, \
                          size=(self.N_x, self.M_v)).astype(np.float32)

  # For each move LEFT, NONE, RIGHT, utility[move,:,:] records the
  # expected utilities of the move in the (pos, vel) states
  def nullify_utilities(self): # initially zero
   #self.utility = np.zeros((self.num_moves, self.N_x, self.M_v))
    self.utility = np.zeros(shape=((self.num_moves, self.N_x, self.M_v)))

  # Visits counts how often a move has been applied in state (pos, vel)
  def nullify_visits(self): # initially zero
   #self.visits = np.zeros((self.num_moves, self.N_x, self.M_v))
    self.visits = np.zeros(shape=((self.num_moves, self.N_x, self.M_v)))

  # Replay list of epsiodes for Monte Carlo learning
  episodes = []

  #------------------------------------------------------------#
  def __init__(self, max_epochs, max_episodes, freeze_delay, max_quotes, \
               num_x_cells, num_v_cells):

    super().__init__(max_epochs, max_episodes, freeze_delay, max_quotes, \
                     num_x_cells, num_v_cells)

    # Set strategies params
    self.randomise_policies()
    self.nullify_utilities()
    self.nullify_visits()

  #------------------------------------------------------------#
  # Gradual freeze out of random fluctuations (large eps = hot, small = cold)
  epsv = [0.9, 0.9, 0.1] # initial epsilon, current epsilon, final epsilon
  rate = 0.9 # new-eps = old-eps * rate ^ {epoch/attenuation}
  def apply_cooling(self, epoch):
    self.epsv[1] = self.epsv[0] * np.power(self.rate, epoch/self.freeze_delay)
    if self.epsv[1] < self.epsv[2]: self.epsv[1] = self.epsv[2]

  #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
  # Learner auxiliaries

  def select_move_from_policy(self, cells):
    '''Select the recommended move with probability near/approaching 1
    (aka "greedy" choice of action)

    @param cell the cell containing the current (pos, vel) state observed
    @return the selected move
    '''
    eps = self.epsv[1] # exceptional probability
    major_prob = 1 - eps
    minor_prob = eps / (self.num_moves - 1)
    mandated = int(self.policy[cells[0], cells[1]])
    mandated = self.index_of_move(mandated) # rebase
    weight_array = np.full((self.num_moves), minor_prob)
    weight_array[mandated] = major_prob
    selected = int(np.random.choice(self.num_moves, 1, p=weight_array))
    selected = self.move_of_index(selected) # unrebase
    return selected

  #------------------------------------------------------------#
  def expected_return(self, downstream_episodes):
    """Get the Expected Return from a sequence of future discounted rewards

    @return the Expected Return
    """
    Return = 0
    for k in range(len(downstream_episodes)):
      stage = downstream_episodes[k]
      reward = stage[2]
      Return += reward * np.power(self.gamma, k)
    return Return

  #------------------------------------------------------------#
  def update_utility(self, cur_cells, new_cells, cur_move, new_move, reward):
    '''Update utilities

    @param utility the matrix before the update
    @param cur_cell the cells containing the (pos, vel) state observed at t
    @param new_cell the cells containing the (pos, vel) state observed at t+1
    @param cur_move the action at t
    @param new_move the action at t+1
    @param reward the reward observed after the action at t
    @return the updated matrix
    '''
    # Get cells containing current and new (pos, vel) pairs
    cur_xc, cur_vc = cur_cells
    new_xc, new_vc = new_cells
    # Get 0 based move indices
    cur_mi = self.index_of_move(cur_move)
    new_mi = self.index_of_move(new_move)
    # Update utility for current (pos, vel) cell pair
    cur_Q = self.utility[cur_mi, cur_xc, cur_vc]
    new_Q = self.utility[new_mi, new_xc, new_vc]
    beta = 1.0 / (1.0 + self.visits[cur_mi, cur_xc, cur_vc])
    self.utility[cur_mi, cur_xc, cur_vc] \
      += beta * (reward + self.gamma * new_Q - cur_Q)

  #------------------------------------------------------------#
  def update_visits(self, cells, move):
    '''Update visit counts

    Keep track on how many times an action pair has been applied in a state
    @param cells the cells containing the (pos, vel) state observed at t
    @param move the action taken at t
    '''
    # Get cell containing current (pos, vel) pairs
    xc, vc = cells
    # Get 0 based move index
    mi = self.index_of_move(move)
    # Update visit count for (pos, vel) cell pair
    self.visits[mi, xc, vc] += 1.0

  #------------------------------------------------------------#
  def update_policy(self, cells):
    '''Update the policy matrix

    @param cells the cells containing the (pos, vel) state observed at t
    @return the updated matrix
    '''
    # Get cell containing current (pos, vel) pairs
    xc, vc = cells
    # Get index with highest utility
    opt_mi = int(np.argmax(self.utility[:, xc, vc]))
    # Get move from index
    opt_move = self.move_of_index(opt_mi)
    # Update policy for (pos, vel) cell pair
    self.policy[xc, vc] = opt_move

  #------------------------------------------------------------#
  def print_policy(self):
    """Print the policy using intuitive symbols < left, o drift, > right

    """
    N_x, M_v = self.policy.shape
    if self.policy is not None:
      policy_string = ""
      for m in range(M_v):
        for n in range(N_x):
          if   self.policy[n,M_v-1-m] == self.LEFT:  policy_string += " <"
          elif self.policy[n,M_v-1-m] == self.DRIFT: policy_string += " o"
          elif self.policy[n,M_v-1-m] == self.RIGHT: policy_string += " >"
          else:                                      policy_string += " ?"
        if m == 0: policy_string += " POS"
        policy_string += '\n'
      policy_string += "VEL\n"
      print(policy_string)

  #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
  # Sundry toys
  def load_policy(self, datasource):
    if os.path.isfile(datasource):
      n = m = 0
      with open(datasource, "r") as fp:
        for line in fp:
          if len(line) == 0: continue
          if line[0] == "#": continue
          tokens = line.strip().split(" ")
          n = len(tokens)
          if n != self.N_x:
            print("Line", m+1, "expected", self.N_x, "tokens, got", n)
            sys.exit()
          for i in range(n):
            if   tokens[i] == "<": policy[i,self.M_v-1-m] = self.LEFT;
            elif tokens[i] == "o": policy[i,self.M_v-1-m] = self.DRIFT;
            elif tokens[i] == ">": policy[i,self.M_v-1-m] = self.RIGHT;
          m += 1
          if m >= self.M_v: break;
      if m != self.M_v:
        print("Expected", self.M_v, "lines, got ", m)
        sys.exit()
    else: # use datasource "0", "1" ... for bottled cocktails
      for v in range(self.M_v):
        # defaults to datasource "0" (go with the flow)
        a = 1
        b = self.N_x-a
        c = d = 0
        if datasource == "1":
          a = v*self.N_x//self.M_v
          b = int(math.ceil(self.M_v-v)*self.N_x/self.M_v)
          c = (self.N_x-2)//2
          d = int(math.ceil(self.N_x+2)/2)
          if c < 0 or d > self.N_x: c = d = 0
        else:
          print("Running with default cocktail...")
        if v < M_v/2:
          for x in range(0,a)       : policy[x,v] = self.DRIFT
          for x in range(a,b)       : policy[x,v] = self.LEFT
          for x in range(b,self.N_x): policy[x,v] = self.DRIFT
        else:
          for x in range(0,c)       : policy[x,v] = self.RIGHT
          for x in range(c,d)       : policy[x,v] = self.DRIFT
          for x in range(d,self.N_x): policy[x,v] = self.RIGHT
    return policy

#================================================================#
# Environments

# Mountain Car
"""A car travels on an inverted cosine curve -- that's a valley with bottom
at (0, -1) between ridges at (-1,1) and (1,1). Half the valley has negative
altitude, on the bit between ordinates -0.5 and 0.5.
"""
class MountainCar(Generics):
  # Position measured along the x-axis (or a suitable geodesic)
  x_min = -1.0 # barrier
  x_max =  1.0 # exit
  x_init = 0.0
  # Velocities in the same directions (rather than the curve)
  v_min = -1.5
  v_max =  1.5
  v_init = 0.0

  def elevation(self, x): # the curve (valley around the origin)
    return -np.cos(np.pi*x)
  def slope_at(self, x): # its slope
    return np.pi*np.sin(np.pi*x)
  def cos_angle_at(self, x): # cosine of its angle with the vertical
    s = self.slope_at(x)
    return s/math.sqrt(1.0+s*s)

  def __init__(self, mass, friction, delta):
    """ Create a new mountain car object.

    @param mass: the mass of the car
    @param friction: its air-resitance
    @param delta: time step-size
    """
    self.gravity = 9.8
    self.friction = friction
    self.delta = delta
    self.mass = mass
    self.amplify = 1.6 # fiddle factor

    self.x_cur = self.x_init
    self.v_cur = self.v_init
    self.y_cur = self.elevation(self.x_cur)

    #self.age = 0
    #self.history = []
    self.last_move = None

  def reset(self, exploring_starts=True, x_init=x_init, v_init=v_init):
    """ Resets the car to an initial position [-1.2, 0.5]

    @param exploring_starts: whether to choose random position at start
    @param x_init: initial position of the car (ignored on exploring_starts)
    @param v_init: initial velocity of the car
    @return: return initial position and velocity of the car
    """
    if exploring_starts: x_init = np.random.uniform(self.x_min, self.x_max)
    if   x_init < self.x_min: x_init = self.x_min
    elif x_init > self.x_max: x_init = self.x_max
    self.x_cur = x_init
    self.v_cur = v_init
    self.y_cur = self.elevation(self.x_cur)
    #self.age = 0
    #self.history = [[None,self.x_cur,self.y_cur,self.v_cur]]
    self.last_move = None
    return [self.x_cur, self.v_cur]

  def step(self, move):
    """Perform one step in the environment following the action.

    @param move: an integer representing one of three moves: LEFT, RIGHT or NONE
    @return: (x_new, v_new), reward, done
     where reward is always negative and done always False until the goal is reached
    """
    assert move in self.moves
    pull = self.gravity * self.mass * self.cos_angle_at(self.x_cur)
    drag = self.friction * self.v_cur
    v_new = self.v_cur + (self.amplify * move - pull - drag) * self.delta
    x_new = self.x_cur + (v_new * self.delta)
    # Enforce boundaries
    if x_new < self.x_min:
      x_new = self.x_min
      v_new = self.v_init
    elif x_new > self.x_max:
      x_new = self.x_max
      v_new = self.v_init
    if v_new < self.v_min:
      v_new = self.v_min
    elif v_new > self.v_max:
      v_new = self.x_max
    # Set new position and velocity
    self.x_cur = x_new
    self.v_cur = v_new
    self.y_cur = self.elevation(self.x_cur)

    # Issue reward and flag completion upon reaching goal, else penalise
    # Note directional non-bias of rewards (go right swinging left first)
    reward = -0.2 # pay rent
    reward += 0.1 * abs(self.x_cur) # reward distance from origin
    reward += 0.1 * abs(self.v_cur) # reward speed
    reward += 0.1 * move*self.v_cur # reward inertia (preserving momentum)
    reward += 0.1 * self.y_cur # reward altitude
    #self.last_move = self.history[-1][0]
    if move != self.last_move: # penalise killing momentum
      if   (self.last_move == self.LEFT  and self.v_cur < -0.001): reward -= 0.1
      elif (self.last_move == self.RIGHT and self.v_cur >  0.001): reward -= 0.1

    #self.age += 1
    #self.history.append([move,self.x_cur,self.y_cur,self.v_cur])
    self.last_move = move

    done = False
    if x_new >= self.x_max:
      reward = 100
      done = True
    return [x_new, v_new], reward, done

#----------------------------------------------------------------#

# Inverted Pendulum
# Based on "Statistical Reinforcement Learning" by Masashi Sugiyama
class CartPole(Generics):
  # Range of angles
  x_max =  np.pi/2.0
  x_min = -x_max
  x_init = 0.0 # vertically up
  # Range of angular velocities
  v_max =  np.pi/2.0
  v_min = -v_max
  v_init = 0.0
  # Freeze params
  epsv = [0.99, 0.99, 0.1]

  def elevation(self, x):
    return -np.cos(x)

  def __init__(self, pole_mass, cart_mass, pole_len, delta):
    """ Create a new pendulum object.

    @param pole_mass: mass of the pole
    @param cart_mass: mass of the cart
    @param pole_len: length of the pole
    @param delta: time step-size
    """
    self.gravity = 9.8
    self.pole_mass = pole_mass
    self.cart_mass = cart_mass
    self.pole_len = pole_len
    self.delta = delta
    self.alpha = 1.0 / (self.pole_mass + self.cart_mass)
    self.amplify = 50.0 # fiddle-factor

    self.x_cur = self.x_init
    self.v_cur = self.v_init

    #self.age = 0
    #self.history = []

  def reset(self, exploring_starts=True, x_init=x_init, v_init=v_init):
    """ Reset the pendulum to an initial position [0, 2*pi]

    @param exploring_starts: if set, random position is taken
    @param x_init: initial position of the pendulum, unless exploring_starts
    @return: initial position and velocity of the pendulum
    """
    if exploring_starts: x_init = np.random.uniform(-np.pi/100.0 , np.pi/100.0)
    if   x_init < self.x_min: x_init = self.x_min
    elif x_init > self.x_max: x_init = self.x_max
    self.x_cur = x_init
    self.v_cur = v_init
    #self.age = 0
    #self.history = [[None,self.x_cur,self.y_cur,self.v_cur]]
    return [self.x_cur, self.v_cur]

  def step(self, move):
    """Perform one step in the environment following the move.

    @param move: an integer representing one of three moves:
     -1=move_left, 0=do_not_move, 1=move_right
    @return: (x_new, v_new), reward, done
    """
    assert move in self.moves
    p = self.gravity * np.sin(self.x_cur) # g \sin\phi
    q = self.pole_mass # m
    q *= self.pole_len # d
    q *= np.power(self.v_cur, 2.0) # (\phi')^2
    q *= np.sin(2.0 * self.x_cur) / 2.0 # 0.5\sin(2\phi)
    r = - np.cos(self.x_cur) # \cos(\phi)
    r *= move * self.amplify # a

    E = (p - (q - r) * self.alpha) * self.delta # \alpha, \Delta t

    p = 4.0 * self.pole_len / 3.0 # 4 l / 3 # but is l == d ?
    q = self.alpha # \alpha
    q *= self.pole_mass # m
    q *= self.pole_len # m
    q *= np.power(np.sin(self.x_cur), 2.0) # \cos^2\phi

    D = p - q

    v_new = self.v_cur
    if abs(D) > 0.00000001: v_new -= E/D
    x_new = self.x_cur + (v_new * self.delta)

    # Check boundary condition
    if x_new < self.x_min:
      x_new = self.x_min
      v_new = self.v_init
    if x_new > self.x_max:
      x_new = self.x_max
      v_new = self.v_init
    if v_new < self.v_min:
      v_new = self.v_min
    if v_new > self.v_max:
      v_new = self.v_max
    # Assign new angle and velocity
    self.x_cur = x_new
    self.v_cur = v_new

    # Issue reward and signal completion
    done = False
    if self.x_cur >= self.x_max or self.x_min >= self.x_cur:
      reward = -100.0
      done = True
    else:
      reward = np.cos(x_new)

    #self.age += 1
    #self.history.append([move,self.x_cur,self.y_cur,self.v_cur])

    return [x_new, v_new], reward, done

#================================================================#
# Pygame utilities

import pygame, time

screen_width = 300
screen_height = 200

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
BLUE = (0, 0, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
YELLOW = (255, 255, 0)

#----------------------------------------------------------------#
def launch_display(caption):
  pygame.init()
  screen = pygame.display.set_mode((screen_width, screen_height))
  pygame.display.set_caption(caption)
  font = pygame.font.SysFont("Calibri", 14, True, False)
  clock = pygame.time.Clock()
  return screen, font, clock

#----------------------------------------------------------------#
def get_interactive_events(halt):
  quit = False
  for event in pygame.event.get():
    if event.type == pygame.QUIT:
      quit = True
    elif event.type == pygame.KEYDOWN:
      if event.key == pygame.K_q:
        quit = True
      elif event.key == pygame.K_RETURN:
        halt = not halt
  return quit, halt

#----------------------------------------------------------------#
def get_screen_text(env, epoch, stage, obs, move, reward, tally):
  data = "  " + str("%d"%epoch) + ":" + str("%d"%stage) + "  "
  if obs is not None and len(obs) > 0:
    data += "( "
    for i in obs: data += str("%.3f "%i)
    data += ")"
  if   move is None:      data += "     "
  elif move == env.LEFT:  data += " <<< "
  elif move == env.DRIFT: data += " ooo "
  elif move == env.RIGHT: data += " >>> "
  data += " " + str("%.3f"%reward) + " "
  data += " " + str("%.3f"%tally) + " "
  return data

#----------------------------------------------------------------#
def paint_scenario(env, cur_obs, cur_move, new_obs, coords_to_screen, \
    epoch, stage, screen, font, clock, reward, tally):

  screen.fill(BLACK)
  x_new, v_new = new_obs
  y_new = env.elevation(x_new)
  new_xs, new_ys = coords_to_screen(env, x_new)
  pygame.draw.circle(screen, BLUE, [new_xs,new_ys], 3, 3)
  if x_new >= env.x_max or env.x_min >= x_new:
    pygame.draw.circle(screen, YELLOW, [new_xs,new_ys], 4, 4)
  elif cur_obs is not None:
    x_cur, _ = cur_obs
    cur_xs, cur_ys = coords_to_screen(env, x_cur)
    pygame.draw.circle(screen, RED, [cur_xs,cur_ys], 3, 3)
    pygame.draw.line(screen, WHITE, [new_xs,new_ys], [cur_xs,cur_ys], 1)
  data = get_screen_text(env, epoch, stage, [x_new, v_new, y_new], \
                         cur_move, reward, tally)
  text = font.render(data, True, WHITE)
  screen.blit(text, [0,0])
  pygame.display.flip()
  clock.tick(5)

#----------------------------------------------------------------#
def paint_mountain_car(env, cur_obs, cur_move, new_obs, \
  epoch, stage, screen, font, clock, halt, reward, tally):

  def cross_section_to_screen(env, x):
    x_orig = screen_width/2
    y_orig = 2*screen_height/3
    x_scale = screen_width/4
    y_scale = screen_height/4
    xs = int(x_orig + x_scale * x)
    ys = int(y_orig - y_scale * env.elevation(x))
    return xs, ys

  paint_scenario(env, cur_obs, cur_move, new_obs, cross_section_to_screen, \
    epoch, stage, screen, font, clock, reward, tally)

  quit, halt = get_interactive_events(halt)

  return quit, halt

#----------------------------------------------------------------#
def paint_pole_cart(env, cur_obs, cur_move, new_obs, \
  epoch, stage, screen, font, clock, halt, reward, tally):

  def upper_semi_circle_to_screen(env, x):
    x_orig = screen_width/2
    y_orig = screen_height/2
    x_scale = screen_width/4
    y_scale = screen_height/4
    xs = int(x_orig + x_scale * np.sin(x))
    ys = int(y_orig - y_scale * np.cos(x))
    return xs, ys

  paint_scenario(env, cur_obs, cur_move, new_obs, upper_semi_circle_to_screen, \
    epoch, stage, screen, font, clock, reward, tally)

  quit, halt = get_interactive_events(halt)

  return quit, halt

#================================================================#
if __name__ == "__main__":
  main()

# vim:set ts=2 sw=2 expandtab indentexpr=

