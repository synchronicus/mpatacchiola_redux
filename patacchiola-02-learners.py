#!/usr/bin/env python

# MIT License
# Copyright (c) 2017 Massimiliano Patacchiola
#               https://mpatacchiola.github.io/blog/
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import getopt, numpy as np, sys

np.set_printoptions(precision=3)
verbose = False

#================================================================#
# Dissecting Reinforcement Learning-Part.2 | mpatacchiola's blog #
#================================================================#

#================================================================#
def usage(x):
  print("Use:\n", x, "[-t<epochs> -s<episodes> -q<prints>] 0|...|7\n" \
        "to invoke a scenario")
  sys.exit()

#----------------------------------------------------------------#

def main():
  scenario = 0

  global max_epochs, max_episodes, max_quotes, verbose

  opts, args = getopt.getopt(sys.argv[1:], "t:s:q:V?")

  if len(args) > 0: scenario = int(args[0])

  for opt in opts:
    if   opt[0] == "-t": max_epochs = int(opt[1])
    elif opt[0] == "-s": max_episodes = int(opt[1])
    elif opt[0] == "-q": max_quotes = int(opt[1])
    elif opt[0] == "-V": verbose = True
    elif opt[0] == "-?": usage(sys.argv[0])
    else: usage(sys.argv[0])

  if max_epochs < 1 or max_episodes < 1 or max_quotes < 0:
    print("Check your params")
    sys.exit()

  if max_quotes > max_epochs: max_quotes = max_epochs

  print("meta-params:", max_epochs, max_episodes, max_quotes, verbose)

  env, opt_policy, alt_policy = cleaning_robot_default()

  if   scenario == 0: test_environment(env, opt_policy)
  elif scenario == 1: monte_carlo_predict(env, opt_policy)
  elif scenario == 2: monte_carlo_generate(env)
  elif scenario == 3: time_difference_nought(env, opt_policy)
  elif scenario == 4: time_difference_lambda(env, opt_policy)
  elif scenario == 5: time_difference_sarsa(env)
  elif scenario == 6: time_difference_qlearn(env, alt_policy)
  elif scenario == 7: actor_critic(env)
  else: usage(sys.argv[0])

#================================================================#
# Environment: Default to cleaning robot world
from gridworldxy import GridWorldxy

def cleaning_robot_default():
  X = 4
  Y = 3

  # Set the environment: The world has area X * Y
  env = GridWorldxy(X, Y)

  # Transition matrix
  # Probabilities that policy set in row i results in action of col j
                         # UP RIGHT DOWN LEFT
  transitions = np.array([[0.8, 0.1, 0.0, 0.1],  # UP
                          [0.1, 0.8, 0.1, 0.0],  # RIGHT
                          [0.0, 0.1, 0.8, 0.1],  # DOWN
                          [0.1, 0.0, 0.1, 0.8]]) # LEFT
  # State matrix
  # Marks world positions as walkable (free), terminal (exit) or forbidden (wall)
  states = np.array([[env.Free, env.Free, env.Free, env.Exit],
                     [env.Free, env.Wall, env.Free, env.Exit],
                     [env.Free, env.Free, env.Free, env.Free]])

  # Reward matrix
  # Reward set to -0.04 for all states but terminal and obstacles
  rewards = np.array([[-0.04, -0.04, -0.04,  1   ], # reward on hitting target
                      [-0.04,  0   , -0.04, -1   ], # reward on getting trapped
                      [-0.04, -0.04, -0.04, -0.04]])

  # Policy matrix (preferred action at position)
  # This is the optimal policy for a world with reward=-0.04
  opt_policy = np.array([[env.Right,  env.Right, env.Right, env.Stop],
                         [env.Up,     env.NoGo,  env.Up,    env.Stop],
                         [env.Up,     env.Left,  env.Left,  env.Left]])

  # Adversarial exploration policy (2 examples)
 #alt_policy = np.array([[env.Down,  env.Left,  env.Down,  env.Stop],
 #                       [env.Down,  env.NoGo,  env.Right, env.Stop],
 #                       [env.Right, env.Right, env.Right, env.Up]])

  alt_policy = np.array([[env.Right, env.Right, env.Right, env.Stop],
                         [env.Up,    env.NoGo,  env.Up,    env.Stop],
                         [env.Up,    env.Right, env.Up,    env.Left]])

  # Convert matrices (row/column indices) to patches (x-y-coordinates)
  states = env.nm_to_xy(states)
  rewards = env.nm_to_xy(rewards)
  opt_policy = env.nm_to_xy(opt_policy)
  alt_policy = env.nm_to_xy(alt_policy)

  if verbose:
    print(transitions)
    env.print_xy(states)
    env.print_xy(rewards)
    env.print_xy(opt_policy)
    env.print_xy(alt_policy)

  # Template the matrices
  env.setTransitionMatrix(transitions)
  env.setStateMatrix(states)
  env.setRewardMatrix(rewards)
  return env, opt_policy, alt_policy

#================================================================#

def test_environment(env, opt_policy):
  # Reset the environment; observed are positions on the gird
  pos = env.reset()
  # Display the world printing on terminal
  env.render()

  while True:
    action = opt_policy[pos[0], pos[1]]
    pos, reward, done = env.step(action)
    print("")
    print("ACTION: " + env.desc_act(action))
    print("REWARD: " + str(reward))
    print("  DONE: " + str(done))
    env.render()
    if done: break

#================================================================#
# Some hyper params
max_epochs = 50000 # number of trials
max_episodes = 1000 # max length of trial
max_quotes = 50 # total number of running stats dumps desired

#----------------------------------------------------------------#
def monte_carlo_predict(env, opt_policy):

  assert opt_policy is not None and opt_policy.shape == (env.X, env.Y)

  gamma = 0.999 # future discount factor (rewards further ahead matter less)

  # Expected Return = sum of the exponentially discounted future rewards
  #                 = sum_{t >= 0} gamma^t * reward[state_t)]
  def expected_return(downstream, gamma):
    Return = 0
    for stage in range(len(downstream)):
      record = downstream[stage]
      reward = record[1] # reward is at index 1
      Return += reward * np.power(gamma, stage)
    return Return # mind-boggling

  # Start with zero utilities
  # Utility is associated with an agent state (i.e., a grid position)
  utility = np.zeros((env.X, env.Y)) # state utility

  # Count inverse visits in order to average cumulative utility per state
  # oo = no visit, 1 = one visit, 0.5 = 2 visits, 0.333... = 3 visits, etc
  rarity = np.full((env.X, env.Y), 1000000.0) # near enough infinity

  print("Initial utility")
  env.print_xy(utility)

  for epoch in range(max_epochs):
    # Start new episode
    episode_stream = list()
    # Reset and return the first observation (starting position)
    pos = env.reset(exploring_starts=False)
    for stage in range(max_episodes):
      x, y = pos
      # Take the action mandated
      action = opt_policy[x,y]
      # Move one step in the environment, collect new position and reward
      pos, reward, done = env.step(action)
      # Record resulting position with reward in episode list
      episode_stream.append((pos, reward))
      if verbose:
        print("Epoch", epoch, "episode", stage, "policy: "+env.desc_act(action), ":", episode_stream)
        env.render()
        if 'q' == input(): sys.exit()
      if done: break
    # Episode finished, now estimate utility
    # Identify stages that are first visits to a position during current epoch
    seen_before = np.zeros((env.X, env.Y))
    # First-Visit MC: For each episode check if the state has been visited prior
    # during the current epoch. If not, accumulate the Return.
    for stage in range(len(episode_stream)):
      record = episode_stream[stage]
      x, y = record[0]
      reward = record[1]
      assert opt_policy[x,y] != env.NoGo
      if seen_before[x,y] == 0: # first visit this episode
        seen_before[x,y] = 1 # mark as visited
        rarity[x,y] = 1.0/(1.0+1.0/rarity[x,y]) # discount further
        Return = expected_return(episode_stream[stage:], gamma) # get Return ...
        utility[x,y] += Return # ... and augment cumulative utility
        if verbose: print("Return at", x, y, ":", Return);
    if max_quotes > 0 and epoch % (max_epochs//max_quotes) == 0 or verbose:
      print("Utilities after iteration", epoch)
      #env.print_xy(utility) # cumulative utility
      #env.print_xy(rarity) # frequency discount
      env.print_xy(utility*rarity) # average utility
      if verbose and 'q' == input(): sys.exit()

  print("Utilities after iteration", max_epochs)
  env.print_xy(utility*rarity)

#----------------------------------------------------------------#
def monte_carlo_generate(env):

  gamma = 0.999 # future discount factor

  def expected_return(downstream, gamma):
    Return = 0
    for stage in range(len(downstream)):
      record = downstream[stage]
      reward = record[2] # reward is now at index 2
      Return += reward * np.power(gamma, stage)
    return Return

  def update_policy(episodes, policy, utility, rarity):
    """ Update a policy
    Makes the policy greedy in respect of the action utility
    @return the updated policies
    """
    for record in episodes:
      x, y = record[0] # position is at index 0
      policy[x,y] = np.argmax(utility[:,x,y]*rarity[:,x,y])
    return policy

  # Start with random policies
  policy = np.random.randint(low=0,high=env.num_acts,
                             size=(env.X,env.Y)).astype(np.float32)

  # Exempt terminal states and obstacles
  for x in range(env.X):
    for y in range(env.Y):
      if env.States[x,y] == env.Exit:
        policy[x,y] = env.Stop # No action type for terminal states
      elif env.States[x,y] == env.Wall:
        policy[x,y] = env.NoGo # No action type for obstacles

  print("Randomised initial policy")
  env.print_actions_xy(policy)

  # Start with random utilities
  # Utility is measured for an action in a given state (position)
  utility = np.random.random_sample((env.num_acts, env.X, env.Y)) # Q/action utility

  # Inverse usage count, in order to average cumulative utility
  rarity = np.full((env.num_acts, env.X, env.Y), 1000000.0)

  # Print cumulative utility of each action, averaged by frequency of use
  def print_action_utility(utility, rarity):
    for a in range(env.num_acts):
      print(env.desc_act(a))
      env.print_xy(utility[a,:,:]*rarity[a,:,:])

  print("Randomised initial utility:")
  print_action_utility(utility, rarity)

  for epoch in range(max_epochs):
    # Start new episode
    episode_stream = list()
    # Reset and return the first observation (starting position)
    new_pos = env.reset(exploring_starts=True)
    is_starting = True
    for stage in range(max_episodes):
      cur_pos = new_pos
      x, y = cur_pos
      # Take the action mandated
      action = int(policy[x,y])
      # If the episode just started then revert to random action (exploring starts)
      if is_starting:
        action = np.random.randint(0, env.num_acts)
        is_starting = False
      # Move one step in the environment, collect new position and reward
      new_pos, reward, done = env.step(action)
      # Record current position with reward/action before making transition
      episode_stream.append((cur_pos, int(action), reward))
      if verbose:
        print("Epoch", epoch, "episode", stage, "policy: "+env.desc_act(action), ":", episode_stream)
        env.render()
        if 'q' == input(): sys.exit()
      if done: break
    # Episode finished, now estimate utility
    # Identify uses of actions in states not seen prior in current episode
    used_before = np.zeros((env.num_acts, env.X, env.Y))
    # Evaluation step of the GPI: For each episode check if the action has been
    # used before during the current epoch. If not, accumulate the Return
    for stage in range(len(episode_stream)):
      record = episode_stream[stage]
      x, y = record[0]
      action = int(record[1])
      assert policy[x,y] != env.NoGo
      if used_before[action,x,y] == 0: # action not used before in state
        used_before[action,x,y] = 1 # mark as used
        rarity[action,x,y] = 1.0/(1.0+1.0/rarity[action,x,y]) # discount
        Return = expected_return(episode_stream[stage:], gamma) # get Return ...
        utility[action,x,y] += Return # ... and accumulate
      if verbose:
        print("Replay: Episode", stage, ", actions used:")
        for a in range(env.num_acts):
          print(env.desc_act(a))
          env.print_xy(used_before[a,:,:])
        if 'q' == input(): sys.exit()
    # Update policy
    policy = update_policy(episode_stream, policy, utility, rarity)

    if max_quotes > 0 and  epoch % (max_epochs//max_quotes) == 0 or verbose:
      print("Utilities after iteration ", epoch)
      print_action_utility(utility, rarity)
      print("Policies after iteration", epoch)
      env.print_actions_xy(policy)
      if verbose and 'q' == input(): sys.exit()

  # At end of time
  print("Utilities after iteration", max_epochs)
  print_action_utility(utility, rarity)
  print("Policies after iteration", max_epochs)
  env.print_actions_xy(policy)

#================================================================#
# Dissecting Reinforcement Learning-Part.3 | mpatacchiola's blog #
#================================================================#

def time_difference_nought(env, opt_policy): # TD0

  assert opt_policy is not None and opt_policy.shape == (env.X, env.Y)

  gamma = 0.999 # future discount factor
  alpha = 0.1 # learning rate (speed of ajustment to change)

  def update_utility(utility, cur_pos, new_pos, reward):
    """Return the updated utilities

    @param utility the state utilities before the update
    @param cur_pos the state observed at t
    @param new_pos the state observed at t+1
    @param reward the reward observed after the action at t
    @return the updated state utilities
    """
    x, y = cur_pos
    u, v = new_pos
    # Add the reward
    delta = reward
    # Add the error estimate = expected return - old estimate
    delta += gamma * utility[u,v] - utility[x,y]

    if verbose:
      print("U(", x, y, ")", utility[x,y],
          ": U(", u, v, ")", utility[u,v],
         "-> U(", x, y, ")", utility[x,y] + alpha * delta)

    # Apply the update
    utility[x,y] += alpha * delta

    return utility

  # Start with zero utilities
  utility = np.zeros((env.X, env.Y)) # state utility

  for epoch in range(max_epochs):
    # Reset and return the first observation (starting position)
    new_pos = env.reset(exploring_starts=True)
    for stage in range(max_episodes):
      cur_pos = new_pos
      x, y = cur_pos
      # Take the action mandated
      action = int(opt_policy[x,y])
      # Move one step in the environment, collect new position and reward
      new_pos, reward, done = env.step(action)
      # Update utility using the TD(0) rule
      utility = update_utility(utility, cur_pos, new_pos, reward)
      if done: break

      if max_quotes > 0 and  epoch % (max_epochs//max_quotes) == 0 or verbose:
        print("Utilities after iteration", epoch)
        env.print_xy(utility)
        if verbose and 'q' == input(): sys.exit()

  print("Utilities after iteration", max_epochs)
  env.print_xy(utility)

#----------------------------------------------------------------#

def time_difference_lambda(env, opt_policy): # TD\lambda

  assert opt_policy is not None and opt_policy.shape == (env.X, env.Y)

  gamma = 0.999 # future discount factor
  alpha = 0.1 # learning rate
  lambda_ = 0.5 # decay factor

  def update_utility(utility, cur_pos, new_pos, reward, traces):
    """Return the updated utilities

    @param utility the state utilities before the update
    @param cur_pos the state observed at t
    @param new_pos the state observed at t+1
    @param reward the reward observed after the action at t
    @param alpha the learning rate
    @param traces the eligibility traces
    @return the updated state utilities
    """
    x, y = cur_pos
    u, v = new_pos
    # Add the reward
    delta = reward
    # Add the error estimate = expected return - old estimate
    delta += gamma * utility[u,v] - utility[x,y]

    if verbose:
      print("U(", x, y, ")", utility[x,y],
          ": U(", u, v, ")", utility[u,v],
         "-> U(", x, y, ")", utility[x,y] + alpha * delta * traces[x,y])

    # Apply the update
    utility[x,y] += alpha * delta * traces[x,y]

    return utility

  def update_eligibility(traces):
    """Return the updated traces

    @param traces the eligibility traces
    @return the updated traces
    """
    traces = traces * gamma * lambda_
    return traces

  # Start with zero utilities
  utility = np.zeros((env.X, env.Y)) # state utility
  traces = np.full((env.X, env.Y), 1)

  for epoch in range(max_epochs):
    # Reset and return the first observation (starting position)
    new_pos = env.reset(exploring_starts=True)
    for stage in range(max_episodes):
      cur_pos = new_pos
      x, y = cur_pos
      # Increment the trace of the state visited
      traces[x,y] += 1
      # Take the action mandated
      action = int(opt_policy[x,y])
      # Move one step in the environment, collect new position and reward
      new_pos, reward, done = env.step(action)
      # Update utility
      utility = update_utility(utility, cur_pos, new_pos, reward, traces)
      # Update trace (apply decay)
      traces = update_eligibility(traces)
      if done: break

      if max_quotes > 0 and  epoch % (max_epochs//max_quotes) == 0 or verbose:
        print("Utilities after iteration", epoch)
        env.print_xy(utility)
        #env.print_xy(traces)
        if verbose and 'q' == input(): sys.exit()

  print("Utilities after iteration", max_epochs)
  env.print_xy(utility)

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
def update_policy(policy, utility, pos):
  """Return the updated policies

  @param policy the policies before the update
  @param utility the utilities
  @param pos the state observed
  @return the updated policies
  """
  x, y = pos
  # Update the policy with the action of highest utility in the given state
  policy[x,y] = np.argmax(utility[:,x,y])
  return policy

def update_usage_count(usage_counts, action, pos):
  """Update the usage counts

  Counting number of times an action has been used in a state
  This information can be used during the update.
  @param usage_counts the frequencies of action use in states
  @param action the action chosen in the state observed
  @param pos the state observed
  """
  x, y = pos
  usage_counts[action,x,y] += 1.0
  return usage_counts

def greedy_action_choice(policy, pos, num_acts, epsilon=0.1):
  """Choose action mandated by policy for current state greedily,
  with probability 1 - epsilon

  @param policy the policies controlling the choice of action
  @param pos the state observed
  @param num_acts the range of available actions codes (restrict to policy?)
  @param epsilon the value used for skewing the choice
  @return the chosen action
  """
  greedy_prob = 1
  marginal_prob = 0
  if num_acts > 1:
    greedy_prob = 1 - epsilon
    marginal_prob = epsilon / (num_acts - 1)
  x, y = pos
  action = int(policy[x,y])
  weight_array = np.full((num_acts), marginal_prob)
  weight_array[action] = greedy_prob
  return np.random.choice(num_acts, 1, p=weight_array)[0]

def apply_decay(start_value, global_step, decay_step, decay_rate=0.1):
  """Returns the decayed value.

  decayed_value = starting_value * decay_rate ^ (global_step / decay_steps)
  @param starting_value the value before decaying
  @param global_step the global step to use for decay (positive integer)
  @param decay_step the step at which the value is decayed
  """
  decayed_value = start_value * np.power(decay_rate, (global_step/decay_step))
  return decayed_value

#----------------------------------------------------------------#

def time_difference_sarsa(env):

  gamma = 0.999 # future discount factor
  alpha = 0.001 # learning rate

  def update_utility(utility, usage_counts, cur_pos, new_pos, cur_act, reward):
    """Return the updated utilities

    @param utility the action utilities before the update
    @param usage_counts a usage count for each action used in a state
    @param cur_pos the state observed at t
    @param new_pos the state observed at t+1
    @param cur_act the action at t
    @param num_acts the legal action range
    @param reward the reward observed after the action
    @return the updated action utilities
    """
    x, y = cur_pos
    u, v = new_pos
    new_act = int(policy[u,v])
    # Add the reward
    delta = reward
    # Add the error estimate = expected return - old estimate
    # The expected return is based on the action mandated for the next step,
    # unless that's terminal
    if new_act >= 0 and  new_act < env.num_acts:
      delta += gamma * utility[new_act,u,v] - utility[cur_act,x,y]
    # Calculate discount based on frequency of action application in state
    beta = 1.0 / (1.0 + usage_counts[cur_act,x,y])

    if verbose:
      if new_act >= 0 and new_act < env.num_acts:
        print("Q(", env.pict_act(cur_act), x, y, ")", utility[cur_act,x,y],
            ": Q(", env.pict_act(new_act), u, v, ")", utility[new_act,u,v],
           "-> Q(", env.pict_act(cur_act), x, y, ")", utility[cur_act,x,y] + beta * delta)
      else:
        print("Q(", env.pict_act(cur_act), x, y, ")", utility[cur_act,x,y],
            ": T(", env.pict_act(env.num_acts), u, v, ")",
           "-> Q(", env.pict_act(cur_act), x, y, ")", utility[cur_act,x,y] + beta * delta)
      if 'q' == input(): sys.exit()

    # Apply the update
    # Use "alpha" in place of "beta" if ignoring usage-counts is desired
    utility[cur_act,x,y] += beta * delta

    return utility

  # Start with random policies
  policy = np.random.randint(low=0,high=env.num_acts,
                             size=(env.X,env.Y)).astype(np.float32)

  # Exempt terminal states and obstacles
  for x in range(env.X):
    for y in range(env.Y):
      if env.States[x,y] == env.Exit:
        policy[x,y] = env.Stop # No action type for terminal states
      elif env.States[x,y] == env.Wall:
        policy[x,y] = env.NoGo # No action type for obstacles

  print("Randomised initial policy")
  env.print_actions_xy(policy)

  # Start with zero utilities
  utility = np.zeros((env.num_acts, env.X, env.Y)) # Q/action utility
  usage_counts = np.zeros((env.num_acts, env.X, env.Y))

  # Print utility of each action
  def print_action_utility(utility):
    for a in range(env.num_acts):
      print(env.desc_act(a))
      env.print_xy(utility[a,:,:])

  for epoch in range(max_epochs):
    epsilon = apply_decay(start_value=0.1, global_step=epoch, decay_step=100000, decay_rate=0.1)
    # Reset and return the first observation (starting position)
    new_pos = env.reset(exploring_starts=True)
    is_starting = True
    for stage in range(max_episodes):
      cur_pos = new_pos
      # Choose mandated action greedily with probability 1 - epsilon
      cur_act = greedy_action_choice(policy, cur_pos, env.num_acts, epsilon=0.1)
      if is_starting:
        cur_act = np.random.randint(0, env.num_acts)
        is_starting = False
      # Move one step in the environment, collect new position and reward
      new_pos, reward, done = env.step(cur_act)
      # Update utility
      utility = update_utility(utility, usage_counts, cur_pos, new_pos, cur_act, reward)
      # Update policy
      policy = update_policy(policy, utility, cur_pos)
      # Increment usage count
      usage_counts = update_usage_count(usage_counts, cur_act, cur_pos)
      if done: break

    if max_quotes > 0 and  epoch % (max_epochs//max_quotes) == 0 or verbose:
      print("Epsilon:", epsilon)
      print("Utilities after iteration", epoch)
      print_action_utility(utility)
      print("Policies after iteration", epoch)
      env.print_actions_xy(policy)
      if verbose and 'q' == input(): sys.exit()

  print("Utilities after iteration", max_epochs)
  print_action_utility(utility)
  print("Policies after iteration", max_epochs)
  env.print_actions_xy(policy)

#----------------------------------------------------------------#

def time_difference_qlearn(env, alt_policy):

  assert alt_policy is not None and alt_policy.shape == (env.X, env.Y)

  gamma = 0.999 # future discount factor
  alpha = 0.001 # learning rate

  def update_utility(utility, usage_counts, cur_pos, new_pos, cur_act, reward):
    """Return the updated action utilities

    @param utility the action utilities before the update
    @param usage_counts a usage count for each action used in a state
    @param cur_pos the state observed at t
    @param new_pos the state observed at t+1
    @param cur_act the action at t
    @param reward the reward observed after the action at t
    @return the updated action utilities
    """
    x, y = cur_pos
    u, v = new_pos
    # Add the reward
    delta = reward
    # Add the error estimate = expected return - old estimate
    # The expected return is based on the action maximising utility
    # at the next step, although that action may not be chosen then
    delta += gamma * np.max(utility[:,u,v]) - utility[cur_act,x,y]
    # Calculate discount based on frequency of action application in state
    beta = 1.0 / (1.0 + usage_counts[cur_act,x,y])

    if verbose:
      print("Q(", env.pict_act(cur_act), x, y, ")", utility[cur_act,x,y],
          ": max Q(", u, v, ")", np.max(utility[:,u,v]),
         "-> Q(", env.pict_act(cur_act), x, y, ")", utility[cur_act,x,y] + beta * delta)
      if 'q' == input(): sys.exit()

    # Apply the update
    # Use "alpha" in place of "beta" if ignoring usage-counts is desired
    utility[cur_act,x,y] += beta * delta

    return utility

  # Start with random policies
  policy = np.random.randint(low=0,high=env.num_acts,
                             size=(env.X,env.Y)).astype(np.float32)

  # Exempt terminal states and obstacles
  for x in range(env.X):
    for y in range(env.Y):
      if env.States[x,y] == env.Exit:
        policy[x,y] = env.Stop # No action type for terminal states
      elif env.States[x,y] == env.Wall:
        policy[x,y] = env.NoGo # No action type for obstacles

  print("Randomised initial policy")
  env.print_actions_xy(policy)

  # Start with zero utilities
  utility = np.zeros((env.num_acts, env.X, env.Y)) # Q/action utility
  usage_counts = np.zeros((env.num_acts, env.X, env.Y))

  # Print utility of each action
  def print_action_utility(utility):
    for a in range(env.num_acts):
      print(env.desc_act(a))
      env.print_xy(utility[a,:,:])

  for epoch in range(max_epochs):
    # Reset and return the first observation (starting position)
    new_pos = env.reset(exploring_starts=True)
    epsilon = apply_decay(start_value=0.1, global_step=epoch, decay_step=50000, decay_rate=0.1)
    is_starting = True
    for stage in range(max_episodes):
      cur_pos = new_pos
      # Choose alternative action greedily with probability 1 - epsilon
      cur_act = greedy_action_choice(alt_policy, cur_pos, env.num_acts, epsilon=0.001)
      if is_starting:
        cur_act = np.random.randint(0, env.num_acts)
        is_starting = False
      # Move one step in the environment, collect new position and reward
      new_pos, reward, done = env.step(cur_act)
      # Update utility
      utility = update_utility(utility, usage_counts, cur_pos, new_pos, cur_act, reward)
      # Update policy
      policy = update_policy(policy, utility, cur_pos)
      # Increment usage count
      usage_counts = update_usage_count(usage_counts, cur_act, cur_pos)
      if done: break

    if max_quotes > 0 and  epoch % (max_epochs//max_quotes) == 0 or verbose:
      print("Epsilon", epsilon)
      print("Utilities after iteration", epoch)
      print_action_utility(utility)
      print("Policies after iteration", epoch)
      env.print_actions_xy(policy)
      if verbose and 'q' == input(): sys.exit()

  print("Utilities after iteration", max_epochs)
  print_action_utility(utility)
  print("Policies after iteration", max_epochs)
  env.print_actions_xy(policy)

#================================================================#
# Dissecting Reinforcement Learning-Part.4 | mpatacchiola's blog #
#================================================================#

def softmax(x):
  '''Compute softmax values of array x.

  @param x the input array
  @return the softmax array
  '''
  return np.exp(x - np.max(x)) / np.sum(np.exp(x - np.max(x)))

def actor_critic(env):

  gamma = 0.999 # future discount factor
  alpha = 0.001 # learning rate

  def update_critic(state_utility, cur_pos, new_pos, reward):
    '''Return the updated state utilities

    @param state_utility the state utilities before the update
    @param cur_pos the state observed at t
    @param new_pos the state observed at t+1
    @param reward the reward observed after the action at t
    @return the updated state utilities
    @return the estimation error delta
    '''
    x, y = cur_pos
    u, v = new_pos
    delta = reward
    delta += gamma * state_utility[u,v] - state_utility[x,y]
    state_utility[x,y] += alpha * delta
    return state_utility, delta

  def update_actor(action_utility, cur_pos, cur_act, delta, usage_counts=None):
    '''Return the updated action utilities

    @param action_utility the action utilities before the update
    @param cur_pos the state observed at t
    @param cur_act taken at time t
    @param delta the estimation error returned by the critic
    @param usage_counts a usage count for each action used in a state
    @return the updated action utilities
    '''
    x,y = cur_pos
    if usage_counts is None: beta = 1.0
    else: beta = 1.0 / usage_counts[cur_act,x,y]
    action_utility[cur_act,x,y] += beta * delta
    return action_utility

  # Start with zero utilities of both kinds
  state_utility = np.zeros((env.X, env.Y)) # state utility
  action_utility = np.zeros((env.num_acts, env.X, env.Y)) # Q/action utility
  usage_counts = np.zeros((env.num_acts, env.X, env.Y))

  # Print utility of each action
  def print_action_utility(utility):
    for a in range(env.num_acts):
      print(env.desc_act(a))
      env.print_xy(utility[a,:,:])

  for epoch in range(max_epochs):
    # Reset and return the first observation (starting position)
    new_pos = env.reset(exploring_starts=True)
    for stage in range(max_episodes):
      cur_pos = new_pos
      # Sample action from softmax
      x, y = cur_pos
      action_array = action_utility[:,x,y]
      action_distribution = softmax(action_array)
      cur_act = np.random.choice(env.num_acts, 1, p=action_distribution)[0]
      if verbose: print(action_array, "->", action_distribution, "->", env.desc_act(cur_act))
      usage_counts[cur_act,x,y] += 1 # increment usage count
      # Move one step in the environment, collect new position and reward
      new_pos, reward, done = env.step(cur_act)
      # Update utilities
      state_utility, delta = update_critic(state_utility, cur_pos, new_pos, reward)
      action_utility = update_actor(action_utility, cur_pos, cur_act, delta, usage_counts)
      if done: break

    if max_quotes > 0 and  epoch % (max_epochs//max_quotes) == 0:
      print("State utilities after iteration", epoch)
      env.print_xy(state_utility)
      print("Action utilities after iteration", epoch)
      print_action_utility(action_utility)

  print("State utilities after iteration", max_epochs)
  env.print_xy(state_utility)
  print("Action utilities after iteration", max_epochs)
  print_action_utility(action_utility)

#================================================================#
if __name__ == "__main__":
  main()

# ----------------------------------------------------------------#
# vim::set ts=2 sw=2 expandtab indentexpr=

