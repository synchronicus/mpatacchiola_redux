# README #

### What is this repository for? ###

* Redux of code accompanying Reinforcement Learning, M.Patacchiola's blog,
  on https://mpatacchiola.github.io/blog/
* Version: Initial

### How do I get set up? ###

* Python3 with NumPy and MatPlotLib
* Optionally PyGame for animations

### Running the code ###

* Read the main routines for options (sometimes option "-?" will show usage). 
  Main is located near the bottom in shorter scripts, near the top in longer.
* Scripts will run without options, but may take time to complete due to
  default trial lengths and numbers.
* For short (but possibly inconclusive) runs use "-t10 -s10 -q5" with no args.
* Add "-V" for verbose output, "-W" for animations where supported.

### Who do I talk to? ###

* synchronicus@yahoo.co.uk
* synchronicus@hotmail.co.uk

