#!/usr/bin/env python

# MIT License
# Copyright (c) 2017 Massimiliano Patacchiola
#               https://mpatacchiola.github.io/blog/
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

#----------------------------------------------------------------#
# Dissecting Reinforcement Learning-Part.6 | mpatacchiola's blog #
#----------------------------------------------------------------#

import numpy as np, random, sys

verbose = False
display = False

#================================================================#
# Environment (Home the drone)

class DronePilot:
  EAST = 0; NORTH = 1; WEST = 2; SOUTH = 3; UP = 4; DOWN = 5; MOVES = 6;
  moves = ["EAST", "NORTH", "WEST", "SOUTH", "UP", "DOWN"]
  num_moves = len(moves)

  def __init__(self, X=5, Y=4, Z=3):
    self.trajectory = list()
    self.X = X
    self.Y = Y
    self.Z = Z
    self.pos_x = np.random.randint(low=0, high=X)
    self.pos_y = np.random.randint(low=0, high=Y)
    self.pos_z = np.random.randint(low=0, high=Z)
    self.home_x = int(X // 2)
    self.home_y = int(Y // 2)
    self.home_z = 0

  def reset(self, exploring_starts=True, init_pos=[0,0,0]):
    if exploring_starts:
      self.pos_x = np.random.randint(low=0, high=self.X)
      self.pos_y = np.random.randint(low=0, high=self.Y)
      self.pos_z = np.random.randint(low=0, high=self.Z)
    else:
      self.pos_x = init_pos[0]
      self.pos_y = init_pos[1]
      self.pos_z = init_pos[2]

    self.trajectory = []  # clear the list
    self.trajectory.append(init_pos)
    return [self.pos_x, self.pos_y, self.pos_z]

  def step(self, move):
    assert move >= 0 and move < self.num_moves
    if   move == self.EAST:  self.pos_x += 1
    elif move == self.NORTH: self.pos_y += 1
    elif move == self.WEST:  self.pos_x -= 1
    elif move == self.SOUTH: self.pos_y -= 1
    elif move == self.UP:    self.pos_z += 1
    elif move == self.DOWN:  self.pos_z -= 1

    # Keep drone in range
    if   self.pos_x >= self.X: self.pos_x = self.X - 1
    elif self.pos_x < 0:       self.pos_x = 0
    if   self.pos_y >= self.Y: self.pos_y = self.Y - 1
    elif self.pos_y < 0:       self.pos_y = 0
    if   self.pos_z >= self.Z: self.pos_z = self.Z - 1
    elif self.pos_z < 0:       self.pos_z = 0

    # Append position to trajectory
    self.trajectory.append([self.pos_x, self.pos_y, self.pos_z])

    done = False
    reward = -0.01
    if self.pos_z == 0: # landed
      done = True # else continue airborne
      if self.pos_x == self.home_x and self.pos_y == self.home_y: # home :)
        reward = 1.0
      else: # off pad :(
        reward = -1.0
    return [self.pos_x, self.pos_y, self.pos_z], reward, done

#================================================================#
# Learning auxiliaries

# Hyperparameters
gamma = 0.999 # futures discount (rewards further ahead are discounted more)
alpha = 0.001 # learning rate (speed of adoption of utility changes)
max_epochs = 10000 # number of trial runs
max_episodes = 100 # length of trial run
max_quotes = 100 # number of periodic prints desired
decay_rate = 0.9 # cooling (heat decay) base rate
freeze_delay = 1000 # freeze attenuation (high = cool slowly, low = cool fast)

#----------------------------------------------------------------#
def update_utility(utility, visits, cur_obs, move, new_obs, reward):
  # Get current Q (at t)
  x, y, z = cur_obs
  cur_Q = utility[move,x,y,z]
  # Estimate subsequent Q (at t+1 --- "if manadated action were taken")
  u, v, w = new_obs
  new_Q = np.amax(utility[:,u,v,w])
  # Learning rate based on how often move was taken in the given state
  # (alternatively could use fixed learning rate alpha)
  beta = 1.0 / (1.0 + visits[move,x,y,z])
  # Apply update rule
  utility[move,x,y,z] += beta * (reward + gamma * new_Q - cur_Q)
  return utility

#----------------------------------------------------------------#
def update_visit_counts(visits, cur_obs, move):
  x, y, z = cur_obs
  visits[move,x,y,z] += 1.0
  return visits

#----------------------------------------------------------------#
def update_policy(policy, utility, cur_obs):
  x, y, z = cur_obs
  # Update policy with action promising highest utility
  opt_move = np.argmax(utility[:,x,y,z])
  policy[x,y,z] = opt_move
  return policy

#----------------------------------------------------------------#
def select_move_from_policy(policy, cur_obs, num_moves, eps):
  # Barring thermal fluctuations return the action with highest utility
  if np.random.uniform(0, 1) < eps: # with decreasing probability deviate ...
    move = np.random.randint(low=0, high=num_moves)
  else: # ... but generally follow policy
    x, y, z = cur_obs
    move = int(policy[x,y,z])
  return move

#----------------------------------------------------------------#
def apply_cooling(eps_init, decay_rate, epoch, freeze_delay):
    # eps = eps_init * decay_rate ^ (epoch/freeze_delays)
    eps = eps_init * np.power(decay_rate, epoch/freeze_delay)
    return eps

#================================================================#
# Pygame utilities

import pygame, time

screen_width = 300
screen_height = 200

BLACK = (0, 0, 0) # ground level
BLUE = (0, 0, 255)
GREEN = (0, 255, 0)
WHITE = (255, 255, 255) # drone ceiling

#----------------------------------------------------------------#
def launch_display(X, Y, caption):
  pygame.init()
  screen = pygame.display.set_mode((screen_width, screen_height))
  pygame.display.set_caption(caption)
  font = pygame.font.SysFont("Calibri", 14, True, False)
  clock = pygame.time.Clock()
  dx = screen_width // (X+2)
  dy = screen_height // (Y+2)
  return screen, font, clock, dx, dy

#----------------------------------------------------------------#
def get_interactive_events(halt):
  quit = False
  for event in pygame.event.get():
    if event.type == pygame.QUIT:
      quit = True
    elif event.type == pygame.KEYDOWN:
      if event.key == pygame.K_q:
        quit = True
      elif event.key == pygame.K_RETURN:
        halt = not halt
  return quit, halt

#----------------------------------------------------------------#
def get_screen_text(env, epoch, stage, obs, move, reward, tally):
  data = "  " + str("%d"%epoch) + ":" + str("%d"%stage) + "  "
  if obs is not None and len(obs) > 0:
    data += "( "
    for i in obs: data += str("%.3f "%i)
    data += ")"
  if   move is None:      data += "   "
  elif move == env.EAST:  data += " E "
  elif move == env.NORTH: data += " N "
  elif move == env.WEST:  data += " W "
  elif move == env.SOUTH: data += " S "
  elif move == env.UP:    data += " U "
  elif move == env.DOWN:  data += " D "
  data += " " + str("%.3f"%reward) + " "
  data += " " + str("%.3f"%tally) + " "
  return data

#----------------------------------------------------------------#
def paint_scenario(env, cur_obs, cur_move, new_obs, X, Y, Z, dx, dy, \
    epoch, stage, screen, font, clock, reward, tally):

  screen.fill(BLACK)
  x, y, z = new_obs # could use self.pos_{x,y,z}
  y = Y-1-y # adjust for top down screen row ordering (and border shift)
  water = BLUE
  home = GREEN
  shade = (z * 255) // (Z+1)
  drone = (shade, shade, shade)
  # For drone translate height into grey shade, else paint water or home patch
  pygame.draw.rect(screen, water, [dx, dy, X*dx, Y*dy])
  pygame.draw.rect(screen, drone, [(x+1)*dx, (y+1)*dy, dx, dy])
  if x != env.home_x or y != env.home_y:
    pygame.draw.rect(screen, home, [(env.home_x+1)*dx, (env.home_y+1)*dy, dx, dy])
  data = get_screen_text(env, epoch, stage, [x, y, z], \
                         cur_move, reward, tally)
  text = font.render(data, True, WHITE)
  screen.blit(text, [0,0])
  pygame.display.flip()
  clock.tick(5)

#================================================================#
# Learner drivers

def run_random_agent_episode(X=5,Y=4,Z=3):
  env = DronePilot(X,Y,Z)
  new_obs = env.reset()
  tally = 0
  print("Starting random agent demo...")
  for stage in range(max_episodes):
    cur_obs = new_obs
    move = np.random.randint(low=0, high=6)
    new_obs, reward, done = env.step(move)
    if verbose:
      print(cur_obs, "%6s"%env.moves[move], "==>", new_obs, reward, done)
    tally += reward
    if done: break
  print("Trial finished after episode", stage+1, "accumulated reward", tally)

#----------------------------------------------------------------#
def run_learner(agent,X=5,Y=4,Z=3):
  env = DronePilot(X,Y,Z)
  num_moves = env.num_moves

  policy = np.random.randint(low=0, high=6, size=(X,Y,Z)).astype(np.float32)
  utility = np.zeros((num_moves,X,Y,Z))
  visits = np.zeros((num_moves,X,Y,Z))

  reward_history = []
  eps_init = 0.9

  quit = halt = False

  if display:
    screen, font, clock, dx, dy = launch_display(X, Y, "Drone (" + agent + ")")

  print("Starting", agent, "...")
  for epoch in range(max_epochs):
    # Reset and return the first observation
    new_obs = env.reset(exploring_starts=True)
    eps = apply_cooling(eps_init, decay_rate, epoch, freeze_delay)
    done = False
    tally = 0
    for stage in range(max_episodes):
      cur_obs = new_obs
      # Select action giving mostly preference to policy
      cur_move = select_move_from_policy(policy, cur_obs, num_moves, eps)
      # Move one step in the environment, get new obs and reward
      new_obs, reward, done = env.step(cur_move)
      if verbose:
        print(cur_obs, "%6s"%env.moves[cur_move], "==>", new_obs, reward, done)
      # Update utilities, policies and visit counts
      utility = update_utility(utility, visits, cur_obs, cur_move, new_obs, \
                               reward)
      policy = update_policy(policy, utility, cur_obs)
      visits = update_visit_counts(visits, cur_obs, cur_move)
      tally += reward

      if display:
        paint_scenario(env, cur_obs, cur_move, new_obs, X, Y, Z, dx, dy, \
          epoch, stage, screen, font, clock, reward, tally)
        quit, halt = get_interactive_events(halt)

      if done or quit: break
      elif halt:       input("...")

    # End of episode
    reward_history.append(tally)
    if epoch == max_epochs-1 or epoch % (max_epochs//max_quotes) == 0:
      print("Epoch", epoch+1, "of", max_epochs,
            "eps %.3f"%eps, "tally %.3f"%tally,
            "Q-min %.3f"%np.amin(utility),
            "Q-max %.3f"%np.amax(utility),
            "Q-mean %.3f\n"%np.mean(utility))
      if False: # TBD
        print("Utilities")
        print(utility)
        print("Policies")
        print(policy)
        print("Visit counts")
        print(visits)

    if quit: break;

  # End of time
  if display:
    time.sleep(3)
    pygame.quit()

#================================================================#
import getopt, sys

def main():
  agents = ["Random-agent", "Q-learner"]
  agent = agents[0]
  global verbose, display
  global max_epochs, max_episodes, max_quotes, decay_rate, freeze_delay

  X = 5; Y = 4; Z = 3

  opts, args = getopt.getopt(sys.argv[1:], "a:n:t:s:q:r:VW")
  for opt in opts:
    if   opt[0] == "-a": agent = agents[int(opt[1])]
    elif opt[0] == "-n": X,Y,Z = map(int, opt[1].split("x"))
    elif opt[0] == "-t": max_epochs = int(opt[1])
    elif opt[0] == "-s": max_episodes = int(opt[1])
    elif opt[0] == "-r": freeze_delay = int(opt[1])
    elif opt[0] == "-q": max_quotes = int(opt[1])
    elif opt[0] == "-V": verbose = True
    elif opt[0] == "-W": display = True

  if max_epochs < 1 or max_episodes < 1 or max_quotes < 0:
    print("Check your params")
    sys.exit()

  if max_quotes > max_epochs: max_quotes = max_epochs

  if   agent == agents[0]: run_random_agent_episode(X,Y,Z)
  elif agent == agents[1]: run_learner(agent,X,Y,Z)

#----------------------------------------------------------------#
if __name__ == "__main__":
  main()

# vim:set ts=2 sw=2 expandtab indentexpr=
